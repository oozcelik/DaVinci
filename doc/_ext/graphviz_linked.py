###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Sphinx extension that wraps sphinx.ext.graphviz HTML output.

The HTML translation handler of sphinx.ext.graphviz is replaced with a wrapper
`html_visit_graphviz_wrapped`. This wraps the HTML produced by
`html_visit_graphviz` with an anchor tag that links to the source of the
original `img`. This is useful is the image to too large to be seen clearly
when embedded in the page.

This extension must be specified in the configuration `extensions` list *after*
sphinx.ext.graphviz.
"""
import logging
import re

from docutils import nodes
from sphinx.ext.graphviz import html_visit_graphviz

logger = logging.getLogger(__name__)


def html_visit_graphviz_wrapped(self, *args, **kwargs):
    format = self.builder.config.graphviz_output_format
    if format != "png":
        # This raises nodes.SkipNode, so execution will stop here
        html_visit_graphviz(self, *args, **kwargs)

    anchor_index = len(self.body)
    anchor_tmp = "REPLACE_ME"
    self.body.append('<a href="{}">'.format(anchor_tmp))
    try:
        html_visit_graphviz(self, *args, **kwargs)
    except nodes.SkipNode:
        # Catch the exception so we can extract and insert the image URL and
        # append the anchor ending before Sphinx continues processing
        src = ""
        for line in self.body:
            if 'src="' in line:
                src, = re.match(".*src=[\"']([^\'\"]+)", line).groups()

        if src:
            self.body[anchor_index] = self.body[anchor_index].replace(
                anchor_tmp, src)
        else:
            logger.warning("Could not find Graphviz URL for injection.")
        self.body.append("</a>")

        raise nodes.SkipNode


def setup(app):
    app.registry.translation_handlers["html"]["graphviz"] = (
        html_visit_graphviz_wrapped, None)

    return {
        "version": "1.0",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
