###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from DaVinci import Options, make_config
from DaVinci.algorithms import add_filter
from PyConf.reading import get_particles
from PyConf.Algorithms import PrintDecayTree


def main(options: Options):
    # Load data from dst onto a "temporary" TES (Transient Event Store) location for a given event cycle.
    # We loop over the algorithms event-by-event, so for given event cycle, TES maps "path" to an "object".
    # For the TES path checkout spruce_passthrough.tck.json or you can do a dst dump
    # (see https://lhcb.github.io/starterkit-lessons/first-analysis-steps/interactive-dst.html)
    #
    turbo_line = "Hlt2BsToJpsiPhi_JPsi2MuMu_PhiToKK_Line"
    input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")

    # Add a filter: We are not really filtering over particles, we are getting over a technical hurdle here.
    # The hurdle being that if the event hasn't fired a HLT2 line then no TES location exists
    # and therefore if any algorithm tries to look for this location, we run into a problem.
    # Side step this issue with a filter, where:
    # - 1st argument is a user defined name.
    # - 2nd argument is the line decision (simply append "Decision" to your HLT2 line name (or inspect hlt2_starterkit.tck.json))
    my_filter = add_filter("HDRFilter_SeeNoEvil", f"HLT_PASS('{turbo_line}')")

    # Defining an algorithm. The alorithm here prints the decaytree
    pdt = PrintDecayTree(name="PrintBsToJpsiPhi", Input=input_data)

    user_algorithms = [my_filter, pdt]

    return make_config(options, user_algorithms)
