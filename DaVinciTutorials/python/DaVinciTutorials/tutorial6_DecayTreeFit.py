###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from DaVinci import Options, make_config
from DaVinci.algorithms import add_filter
from PyConf.reading import get_particles, get_pvs, get_pvs_v1

from FunTuple import FunctorCollection as FC
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple.functorcollections import Kinematics


def main(options: Options):
    """
    For more information on DTF see:
    - https://inspirehep.net/literature/679286
    - https://twiki.cern.ch/twiki/bin/view/LHCb/DecayTreeFitter
    - https://www.nikhef.nl/~wouterh/topicallectures/TrackingAndVertexing/part6.pdf
    """
    from DecayTreeFitter import DecayTreeFitter

    #Define a dictionary of "field name" -> "decay descriptor component".
    fields = {
        "Bs": "B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)",
        "Jpsi": "B_s0 ->^(J/psi(1S) -> mu+ mu-)  (phi(1020) ->K+ K-)",
        "Phi": "B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) ->K+ K-)",
    }

    #Load data from dst onto a TES (See Example7)
    turbo_line = "Hlt2BsToJpsiPhi_JPsi2MuMu_PhiToKK_Line"
    input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")

    #get kinematic functors
    kin = Kinematics()

    ####### Mass constraint
    #For DecayTreeFitter, as with MC Truth algorithm (previous example), this algorithm builds a relation
    # table i.e. one-to-one map b/w B candidate -> Refitted B candidate.
    # The relation table is output to the TES location "DTF.Algorithm.OutputRelations"
    # You can apply functors to refitted candidates using get_info member function.
    # Note: the Jpsi constraint is applied but the phi constraint seems not to be applied (see issue: https://gitlab.cern.ch/lhcb/Rec/-/issues/309)
    DTF = DecayTreeFitter(
        name='DTF', mass_constraints=["J/psi(1S)"], input=input_data)

    #Loop over the functors in kinematics function and create a new functor collection
    dtf_kin = FC({
        'DTF_' + k: DTF.get_info(v)
        for k, v in kin.get_thor_functors().items()
    })
    #print(dtf_kin)
    #########

    ####### Mass constraint + primary vertex constraint
    #Load PVs onto TES from data. Note here that we call "make_pvs_v1()" to pass to DTF algorithm and "make_pvs()" is passed to ThOr functors.
    # The function "make_pvs()" returns v2 vertices whereas "make_pvs_v1()" returns v1 verticies.
    # The PV constraint in the Decay tree fitter currently only works with v1
    # (see https://gitlab.cern.ch/lhcb/Rec/-/issues/318 and https://gitlab.cern.ch/lhcb/Rec/-/issues/309)
    pvs = get_pvs_v1()
    pvs_v2 = get_pvs()

    #Add not only mass but also constrain Bs to be coming from primary vertex
    DTFpv = DecayTreeFitter(
        name='DTFpv',
        input_pvs=pvs,
        mass_constraints=["J/psi(1S)", "phi(1020)"],
        input=input_data)

    #define the functors
    pv_fun = {}
    pv_fun['BPVLTIME'] = F.BPVLTIME(pvs_v2)
    pv_fun['BPVIPCHI2'] = F.BPVIPCHI2(pvs_v2)
    pv_coll = FC(pv_fun)

    #We now take the pre-defined functor collection ("pv_fun") and add same variables to it
    # but using the result of the decay tree fit (DTF). These variables will have the prefix ("DTFPV_").
    # The resolution on the B candidate lifetime post-DTF ("DTFPV_BPVLTIME")
    # should have improved compared to lifetime variable pre-DTF ("BPVLTIME").
    # Below we make use of the helper function ("DTFPV_MAP") defined previously.
    pv_coll += FC({
        'DTFPV_' + k: DTFpv.get_info(v)
        for k, v in pv_coll.get_thor_functors().items()
    })

    #Define variables dictionary "field name" -> Collections of functor
    variables = {"ALL": kin + dtf_kin, "Bs": pv_coll}

    #Add a filter (See Example7)
    my_filter = add_filter("HDRFilter_SeeNoEvil", f"HLT_PASS('{turbo_line}')")

    #Define instance of FunTuple
    mytuple = Funtuple(
        "TDirectoryName",
        "TTreeName",
        fields=fields,
        variables=variables,
        inputs=input_data)

    config = make_config(options, [my_filter, mytuple])
    return config
