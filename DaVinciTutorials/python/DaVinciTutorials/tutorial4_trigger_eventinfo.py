###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from DaVinci import Options, make_config
from DaVinci.algorithms import add_filter
from PyConf.reading import get_particles
from FunTuple import FunctorCollection as FC
from FunTuple import FunTuple_Particles as Funtuple


def main(options: Options):
    #Define a dictionary of "field name" -> "decay descriptor component".
    fields = {"Bs": "B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)"}

    #To help users, there are pre-defined FunctorCollections (Tuple-tool like objects for Run1/2 veterans) that you can import and inspect.
    # Here we import a pre-defined FunctorCollection "Kinematics".
    # One can call "print(help(Kinematics))" (you have to press "q" to exit after calling) to check the usage and their arguments.
    # Functors that have data dependency will naturally induce data dependency on the functorcollections.
    #
    # To see what functor collections are available see: https://gitlab.cern.ch/lhcb/Analysis/-/blob/master/Phys/FunTuple/python/FunTuple/functorcollections.py
    from FunTuple.functorcollections import Kinematics

    #Inspect whats in the collection by printing
    kin = Kinematics()
    print(kin)

    #Define new collection
    coll = FC({"ID": F.PARTICLE_ID})

    #Add to existing collections (can also subtract two collections)
    kin += coll

    #Remove from collections
    kin.pop(['PX', 'PT', 'PZ', 'PY', 'ENERGY'])
    print(kin)

    #Can also obtain a pure dictionary from collections via
    # - kin.functor_dict (Contains both LoKi and ThOr)
    # - kin.get_thor_functors()
    # - kin.get_loki_functors()
    print(kin.functor_dict)
    print(kin.get_thor_functors())
    # empty dictionary since we have no LoKi functors in the collection
    print(kin.get_loki_functors())

    #Now import two other pre-defined FunctorCollections: SelectionInfo and EventInfo
    # - SelectionInfo: Contains functors related to storing Hlt1, Hlt2 or Sprucing trigger line decision and Trigger Configuration Key (TCK).
    # - EventInfo: Contains functors related to storing event information EVENTNUMBER, RUNNUMBER, GPSTIME, etc.
    #
    #As before you can call help with "print(help(EventInfo))" or "print(help(SelectionInfo))" (you have to press "q" to exit after calling)
    from FunTuple.functorcollections import SelectionInfo, EventInfo

    #Get event information like RUNNUMBER, EVENTNUMBER.
    # These are stored in "LHCb::ODIN" C++ object which the ThOr functors take as input (like PVs in Example7), load it onto TES using "get_odin".
    # The attribute extra_info is False by default, if set to "True" you get info on
    # bunchcrossing id, ODIN TCK, GPS Time, etc
    evtinfo = EventInfo(extra_info=False)
    print(evtinfo)

    #Get selection line decision and HlT2 TCK.
    # These decisions are stored in "LHCb::HltDecReports" object, which the ThOr functors take as input (like PVs in Example7), load it onto TES using "get_decreports".
    # The function "get_decreports" takes as input:
    #  - sel_type: Type of selection "Hlt2" or "Spruce"
    #  - line_names: list of line decision in this instance HLT2 line. Should return True for all since we are using the output of this line.
    #
    # The `Hlt1` decisions can be stored in similar way to `Hlt2` and `Spruce`
    # (see example `option_trigger_decisions` in `DaVinciExamples` folder).
    # For details, can also refer to the [talk](https://indico.cern.ch/event/1164051/#5-accessing-hlt1-decisions-usi)
    # (The talk mentions that to persist Hlt1 decisions, one needs to add few options to the Moore script).
    sel_type = "Hlt2"  #User defined and will be used as prefix for TBranch in the root file
    turbo_line = "Hlt2BsToJpsiPhi_JPsi2MuMu_PhiToKK_Line"
    turbo_line2 = "Hlt2BsToJpsiPhi_JPsi2ee_PhiToKK_Line"
    line_names = [f'{turbo_line}Decision', f'{turbo_line2}']
    selinfo = SelectionInfo(sel_type, line_names)
    print(selinfo)

    #Define variables dictionary "field name" -> Collections of functor
    variables = {"ALL": kin}

    #Load data from dst onto a TES
    input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")

    #Add a filter
    my_filter = add_filter("HDRFilter_SeeNoEvil", f"HLT_PASS('{turbo_line}')")

    #Define instance of FunTuple
    mytuple = Funtuple(
        "TDirectoryName",
        "TTreeName",
        fields=fields,
        variables=variables,
        event_variables=evtinfo + selinfo,
        inputs=input_data)

    config = make_config(options, [my_filter, mytuple])
    return config
