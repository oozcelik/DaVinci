###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from DaVinci import Options, make_config
from DaVinci.algorithms import add_filter
from PyConf.reading import get_particles
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple.functorcollections import Kinematics


def main(options: Options):
    #Define a dictionary of "field name" -> "decay descriptor component".
    fields1 = {
        "Bs": "B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)",
        "mup": "B_s0 -> (J/psi(1S) -> ^mu+ mu-)  (phi(1020) ->K+ K-)",
        "mum": "B_s0 -> (J/psi(1S) -> mu+ ^mu-) (phi(1020) ->K+ K-)",
    }
    fields2 = {
        "Bs": "B_s0 -> (J/psi(1S) ->  e+  e-) (phi(1020) ->K+ K-)",
        "ep": "B_s0 -> (J/psi(1S) -> ^e+  e-) (phi(1020) ->K+ K-)",
        "em": "B_s0 -> (J/psi(1S) ->  e+ ^e-) (phi(1020) ->K+ K-)"
    }

    #Define variables dictionary "field name" -> Collections of functor
    variables = {"ALL": Kinematics()}

    #Load data from dst onto a TES
    turbo_line1 = "Hlt2BsToJpsiPhi_JPsi2MuMu_PhiToKK_Line"
    input_data1 = get_particles(f"/Event/HLT2/{turbo_line1}/Particles")
    my_filter1 = add_filter("HDRFilter_SeeNoEvil1",
                            f"HLT_PASS('{turbo_line1}')")
    mytuple1 = Funtuple(
        "TDirectoryName1",
        "TTreeName1",
        fields=fields1,
        variables=variables,
        inputs=input_data1)

    # If running over several sprucing lines (e.g. for calibration) one can define multiple instances of FunTuple
    turbo_line2 = "Hlt2BsToJpsiPhi_JPsi2ee_PhiToKK_Line"
    input_data2 = get_particles(f"/Event/HLT2/{turbo_line2}/Particles")
    my_filter2 = add_filter("HDRFilter_SeeNoEvil2",
                            f"HLT_PASS('{turbo_line2}')")
    mytuple2 = Funtuple(
        "TDirectoryName2",
        "TTreeName2",
        fields=fields2,
        variables=variables,
        inputs=input_data2)

    user_algorithms = {
        "Alg1": [my_filter1, mytuple1],
        "Alg2": [my_filter2, mytuple2],
    }
    return make_config(options, user_algorithms)
