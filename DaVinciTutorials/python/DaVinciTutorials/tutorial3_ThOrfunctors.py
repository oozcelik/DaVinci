###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from DaVinci import Options, make_config
from DaVinci.algorithms import add_filter
from PyConf.reading import get_particles, get_pvs
from FunTuple import FunctorCollection as FC
from FunTuple import FunTuple_Particles as Funtuple


def main(options: Options):
    #Define a dictionary of "field name" -> "decay descriptor component".
    # Can get daughter information from the head of the decay using F.CHILD functors see below.
    fields = {
        "Bs": "B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)",
        "Phi": "B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) ->K+ K-)",
        "Kp": "B_s0 ->  (J/psi(1S) -> mu+ mu-) (phi(1020) ->^K+ K-)",
        "Km": "B_s0 ->  (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ ^K-)",
    }

    #Load PVs onto TES from data, like we did with input_data below
    # Creating v2 reconstructed vertices to be used in the following functor
    # For the time being there's a mix of legacy and v2 event classes. That will eventually be cleaned once the
    # event model is fixed. In the meantime there are helper functions in DaVinci.
    pvs = get_pvs()

    #Evaluate the impact parameter
    all_vars = {}
    #The ThOr functor F.BPVIPCHI2 is data dependent. It takes as input list of pvs.
    #It calculates impact parameter chisq wrt best PV.
    # - best PV is the PV which fits best the FD of the B candidate.
    # - impact parameter chisq is the difference in the vertex-fit chisq of a given PV reconstructed with and w/o the track under consideration.
    all_vars['BPVIPCHI2'] = F.BPVIPCHI2(pvs)

    #define dictionary
    bs_vars = {}

    #Tupling vector functors
    # Some functors could also return std::map<std::string, std::any> (e.g. F.DECISIONS(Lines=line_names, DecReports=dec_report))
    bs_vars['BPVFDVEC_'] = F.BPVFDVEC(pvs)  #Returns 3-vector
    bs_vars['FOURMOM_P'] = F.FOURMOMENTUM  #Returns 4-vector

    #define some helpful lambda function to simplify syntax
    # This is bit like LoKi preamble of renaming functors that we encountred in previous tutorial.
    CHILD_1 = lambda func: F.CHILD(1, func)
    CHILD_2 = lambda func: F.CHILD(2, func)
    SUBCOMB_12 = lambda func: F.SUBCOMB(Functor=func, Indices=(1, 2))

    #Store the ID of the two daughters of B_s0
    bs_vars['jpsi_ID'] = CHILD_1(F.PARTICLE_ID)
    bs_vars['phi_ID'] = CHILD_2(F.PARTICLE_ID)
    bs_vars['Kp_ID'] = CHILD_2(CHILD_1(F.PARTICLE_ID))

    #Calculate sum of pT of jpsi daughter tracks
    bs_vars['jpsi_TRACKSUMPT'] = CHILD_1(F.SUM(F.PT))

    #Calculate impact parameter of K+
    bs_vars['Kp_BPVIP'] = CHILD_2(CHILD_1(F.BPVIP(pvs)))

    #Calculate invariant mass of K+ and K- combination
    bs_vars['phi_M_comb'] = CHILD_2(SUBCOMB_12(F.MASS))

    #Calculate the difference in end vertex between phi and Bs
    bs_vars['Delta_END_VZ_PhiBs0'] = CHILD_2(F.END_VZ) - F.END_VZ

    #Calculate inv mass of K+pi- where the K- is given the mass hypothesis of pi-
    bs_vars['phi_mass_kpi'] = CHILD_2(F.MASSWITHHYPOTHESES(('K+', 'pi-')))

    #Calculate inv mass of K+K-
    # There three functors for computing this i.e. F.MASS, CHILD_2(SUBCOMB_12(F.MASS)) and CHILD_2(F.MASSWITHHYPOTHESES(('K+', 'K-'))) but why?
    # (see issue: https://gitlab.cern.ch/lhcb/Rec/-/issues/307)
    bs_vars['phi_mass_kk'] = CHILD_2(F.MASSWITHHYPOTHESES(('K+', 'K-')))
    all_vars['M'] = F.MASS

    #Define variables dictionary "field name" -> Collections of functor
    variables = {
        "ALL": FC(all_vars),
        "Bs": FC(bs_vars),
    }

    #Load data from dst onto a TES
    turbo_line = "Hlt2BsToJpsiPhi_JPsi2MuMu_PhiToKK_Line"
    input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")

    #Add a filter
    my_filter = add_filter("HDRFilter_SeeNoEvil", f"HLT_PASS('{turbo_line}')")

    #Define instance of FunTuple
    mytuple = Funtuple(
        "TDirectoryName",
        "TTreeName",
        fields=fields,
        variables=variables,
        inputs=input_data)

    config = make_config(options, [my_filter, mytuple])
    return config
