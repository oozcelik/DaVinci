###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from DaVinci import Options, make_config
from DaVinci.algorithms import add_filter
from PyConf.reading import get_particles
from FunTuple import FunctorCollection as FC
from FunTuple import FunTuple_Particles as Funtuple


def main(options: Options):
    #Define a dictionary of "field name" -> "decay descriptor component".
    fields = {
        "Bs": "B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)",
        "Jpsi": "B_s0 ->^(J/psi(1S) -> mu+ mu-)  (phi(1020) ->K+ K-)",
        "Phi": "B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) ->K+ K-)",
        "Mup": "B_s0 ->  (J/psi(1S) ->^mu+ mu-) (phi(1020) ->K+ K-)",
        "Mum": "B_s0 ->  (J/psi(1S) -> mu+ ^mu-) (phi(1020) ->K+ K-)",
        "Kp": "B_s0 ->  (J/psi(1S) -> mu+ mu-) (phi(1020) ->^K+ K-)",
        "Km": "B_s0 ->  (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ ^K-)",
    }

    #Import FunctorCollections Kinematics, MCKinematics, MCHierarchy
    # There is also "MCVertexInfo" but we won't import it here.
    #
    # See whats available at: https://gitlab.cern.ch/lhcb/Analysis/-/blob/master/Phys/FunTuple/python/FunTuple/functorcollections.py
    from FunTuple.functorcollections import Kinematics, MCKinematics, MCHierarchy

    #We can seek help on these functorcollections using following commands (if you run these commmands, press "q" to exit and continue).
    # - print(help(MCKinematics))
    # - print(help(MCHierarchy))
    #
    # We see that it takes an input an algorithm configured_MCTruthAndBkgCatAlg(inputdata), so lets import that.
    from DaVinci.truth_matching import configured_MCTruthAndBkgCatAlg

    #Load data from dst onto a TES
    turbo_line = "Hlt2BsToJpsiPhi_JPsi2MuMu_PhiToKK_Line"
    input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")
    #Define an algorithm that builds a map i.e. one-to-one relation b/w Reco Particle -> Truth MC Particle.
    mctruth = configured_MCTruthAndBkgCatAlg(inputs=input_data)
    #print(mctruth.MCAssocTable)

    #Pass it to collections
    kin = Kinematics()
    mckin = MCKinematics(mctruth)
    mchierarchy = MCHierarchy(mctruth)
    #print(mckin)
    #print(mchierarchy)

    #Loop over and keep only whats required
    kin = FC({
        k: v
        for k, v in kin.get_thor_functors().items() if k == 'P' or k == 'M'
    })
    mckin = FC(
        {k: v
         for k, v in mckin.get_thor_functors().items() if k == 'TRUEP'})
    mchierarchy = FC({
        k: v
        for k, v in mchierarchy.get_thor_functors().items() if k == 'TRUEID'
    })
    #print(kin)
    #print(mckin)
    #print(mchierarchy)

    #To get truth information with a functor that is not present in the collections
    # - The 1st argument is the functor that returns the relevant information
    # - The 2nd argument is relation table i.e. a one-to-one map b/w Reco Particle -> Truth MC Particle.
    # Lets create a helper lambda function for that
    MCTRUTH = lambda func: F.MAP_INPUT(func, mctruth.MCAssocTable)
    extra_info = {
        # Important note: specify an invalid value for integer functors if there exists no truth info.
        #                 The invalid value for floating point functors is set to nan.
        "TRUEEID": F.VALUE_OR(0) @ MCTRUTH(F.PARTICLE_ID),
        "TRUEEPHI": MCTRUTH(F.PHI),
    }
    extra_info = FC(extra_info)

    #The algorithm mctruth also outputs a map b/w particle and bkg category which can be obtained using the functor
    # For more info on background category see: https://twiki.cern.ch/twiki/bin/view/LHCb/TupleToolMCBackgroundInfo
    bkg_cat = FC({"BKGCAT": F.BKGCAT(Relations=mctruth.BkgCatTable)})

    #Define variables dictionary "field name" -> Collections of functor
    variables = {"ALL": kin + mckin + mchierarchy + bkg_cat, "Kp": extra_info}

    #Add a filter
    my_filter = add_filter("HDRFilter_SeeNoEvil", f"HLT_PASS('{turbo_line}')")

    #Define instance of FunTuple
    mytuple = Funtuple(
        "TDirectoryName",
        "TTreeName",
        fields=fields,
        variables=variables,
        inputs=input_data)

    config = make_config(options, [my_filter, mytuple])
    return config
