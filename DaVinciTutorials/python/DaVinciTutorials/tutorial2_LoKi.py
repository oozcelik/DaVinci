###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from DaVinci import Options, make_config
from DaVinci.algorithms import add_filter
from PyConf.reading import get_particles
from FunTuple import FunctorCollection as FC
from FunTuple import FunTuple_Particles as Funtuple


def main(options: Options):
    # Define a dictionary of "field name" -> "decay descriptor component".
    fields = {
        "Bs": "B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)",
        "Jpsi": "B_s0 -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)",
        "Phi": "B_s0 ->  (J/psi(1S) -> mu+ mu-) ^(phi(1020) ->K+ K-)",
        "Mup": "B_s0 ->  (J/psi(1S) ->^mu+ mu-) (phi(1020) ->K+ K-)",
        "Mum": "B_s0 ->  (J/psi(1S) -> mu+ ^mu-) (phi(1020) ->K+ K-)",
        "Kp": "B_s0 ->  (J/psi(1S) -> mu+ mu-) (phi(1020) ->^K+ K-)",
        "Km": "B_s0 ->  (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ ^K-)",
    }

    # Define a collection of functors called FunctorCollection, which takes dictionary of "variable name" -> "LoKi" or "ThOr" functor
    # For more info on ThOr see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors.html#functor-cache
    # For list of ThOr functors see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors_reference.html
    # For information on LoKi functor see https://lhcb.github.io/starterkit-lessons/first-analysis-steps/loki-functors.html
    mom_fun = FC({
        "THOR_PT": F.PT,
        "THOR_PX": F.PX,
        "THOR_PY": F.PY,
        "LOKI_PT": 'PT',  # LoKi functor code is represented in a string
        "LOKI_PX": 'PX',
        "LOKI_PY": 'PY'
    })

    # Define a LoKi preamble (Note that one can define preambles in ThOr using python lambda function see next tutorial or via FunctorComposition)
    # i.e. rename a complex LoKi functor to a user deinfed name (e.g. TRACK_MAX_PT)
    # This helps us to use "TRACK_MAX_PT" when constructing FunctorCollection
    loki_preamble = ['TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)']

    # Define collections to be added to fields
    max_pt_fun = FC({
        # With LoKi
        "MAX_PT_LOKI": "TRACK_MAX_PT",
        # ThOr (not equivalent, sum of pT of composites not basic). MAXTREE ThOr doesn't exist yet.
        "MAX_PT_THOR": F.MAX(F.PT),
    })

    # Define variables dictionary "field name" -> Collections of functor.
    variables = {
        "ALL": mom_fun,
        "Bs": max_pt_fun,
        "Jpsi": max_pt_fun,
        "Phi": max_pt_fun,
    }

    #Load data from dst onto a TES
    turbo_line = "Hlt2BsToJpsiPhi_JPsi2MuMu_PhiToKK_Line"
    input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")

    #Add a filter
    my_filter = add_filter("HDRFilter_SeeNoEvil", f"HLT_PASS('{turbo_line}')")

    #Define instance of FunTuple
    mytuple = Funtuple(
        "TDirectoryName",
        "TTreeName",
        fields=fields,
        variables=variables,
        loki_preamble=loki_preamble,  #optional argument
        inputs=input_data)

    config = make_config(options, [my_filter, mytuple])
    return config
