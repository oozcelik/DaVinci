# DaVinci Tutorials

The tutorials have originally been developed for the LHCb [starter-kit](https://indico.cern.ch/event/1124730/timetable/#20220316) that took place in March 2022.
The tutorials are accompanied by [slides](https://indico.cern.ch/event/1124730/timetable/#20220316) and [recordings](https://videos.cern.ch/record/2295643).
Note that in the slides and recordings, the tutorials have been labelled differently to what is present here (the slides and recordings will be updated for the next `starterkit` lesson).

## Setup

To setup using the nightlies use:

```bash
lb-run --nightly=lhcb-master/Today DaVinci/v60r3 bash
```

Alternatively, if you need to make non-trivial changes build your own [stack](https://gitlab.cern.ch/rmatev/lb-stack-setup) for DaVinci (WARNING: Takes a very long time to build and is not suited to virtual machines like lxplus)

```bash
# Go to home or any directory of your choice
cd $HOME
# Setup the "stack/" folder
curl https://gitlab.cern.ch/rmatev/lb-stack-setup/raw/master/setup.py | python3 - stack
# compile DaVinci (DV) master
cd stack
make DaVinci
DaVinci/run bash
```

In the lesson, we will be using the `Turbo` upgrade simulation sample analysing the decays of `Bs0->J/psi (-> mu+ mu-) phi (-> K+ K-)`.
The simulation samples can either be in the local directory or at CERN EOS.
Here our example DST file (`hlt2_passthrough_thor_lines.dst`) and accompanying configuration file (`hlt2_passthrough_thor_lines.tck.json`) are both at CERN EOS (see `jobopts.yaml` file).

<!--For the hands-on session, we will be using a 'Spruce' upgrade simulation sample analysing the decays of `Bc -> Bs0 pi+`. Simulation sample obtained from Spruce line output (`spruce_exclusive_BcToBspi.dst`) and configuration file (`spruce_exclusive.tck.json`) are also at CERN EOS (see `jobopts_spruce.yaml` file).-->

## Tutorial0: Running a simple DaVinci job

Objectives:

- Creating an `options.yaml` file that configures the job
- Create a function that returns a sequence of user defined algorithms
- Running the example using `lbexec`

Command to run the tutorial:

```bash
lbexec DaVinciTutorials.tutorial0_basic_DVjob:main "$DAVINCITUTORIALSROOT/options.yaml"
```

## Tutorial1: FunTuple basic ThOr functors and special field name

Objectives:

- Basic configuration of `FunTuple`.
- Defining a collection of `ThOr` functors (`FunctorCollection`).
- Configuring the `fields` attribute of `FunTuple`.
- Usage of special field name `ALL`.
- Inspecting C++ string representation of `ThOr` functors.
- Loading particles in the event from `.dst` onto Transient Event Store (TES) location.
- Usage of event filter (mainly to get over a technical hurdle).

To see particle properties (for names, properties, etc):

```bash
dump_ParticleProperties -t Upgrade | tee ParticleTable.txt
```

Command to run the tutorial:

```bash
lbexec DaVinciTutorials.tutorial1_functors_specialfield:main "$DAVINCITUTORIALSROOT/options.yaml"
```

External links:

- Decay descriptors: https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
- `ThOr` documentation: https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors.html
- [List](https://gitlab.cern.ch/lhcb/Rec/-/blob/master/Phys/FunctorCore/python/Functors/__init__.py) of available `ThOr` functors.

## Tutorial2:  LoKi functors

Objectives:

- Defining a collection of `LoKi` functors together with `ThOr` functors.
- Defining a LoKi preamble for a complex LoKi functor to be used in `FunctorCollection`.

Command to run the tutorial:

```bash
lbexec DaVinciTutorials.tutorial2_LoKi:main "$DAVINCITUTORIALSROOT/options.yaml"
```

External links:

- `LoKi` official page: http://lhcb-comp.web.cern.ch/Analysis/Loki/index.html
- `LoKi` starter-kit page: https://lhcb.github.io/starterkit-lessons/first-analysis-steps/loki-functors.html

## Tutorial3:  ThOr functors (Data dependence, arguments and return types)

Objectives:

- Loading primary vertices (PVs) onto TES, which is passed to data dependent `ThOr` functors in `FunTuple` e.g. `F.BPVIPCHI2(pvs)`.
- Functors returning three and four vectors e.g. `F.BPVFDVEC(pvs)` that returns `3-vector`.
- Usage of functors taking other arguments such as other functors `func` e.g. `F.CHILD(1,func)`, `F.SUM(func)`, `F.SUMCOMB(func,Indices)`, `F.MASSWITHHYPOTHESES(('K+', 'K-')`.
- Basic maths operators with functor returning scalars e.g. `CHILD_2(F.END_VZ) - F.END_VZ`, which returns difference in end vertex of child and mother.

Command to run the tutorial:

```bash
lbexec DaVinciTutorials.tutorial3_ThOrfunctors:main "$DAVINCITUTORIALSROOT/options.yaml"
```

## Tutorial4: Usage of pre-defined `functorcollections`, storing trigger and event-level information


Objectives:

- Usage of pre-defined `functorcollections`, inspecting and manipulating them before loading it onto `FunTuple`.
- Exploring few simple methods of `FunctorCollection` class.
- Storing event-level information with `functorcollection` e.g. RunNumber, EventNumber, etc.
- Storing trigger (`Hlt1`, `Hlt2` and `Sprucing`) information with `functorcollection` e.g. line decisions, Trigger Configuration Key (TCK).

Command to run the tutorial:

```bash
lbexec DaVinciTutorials.tutorial4_trigger_eventinfo:main "$DAVINCITUTORIALSROOT/options.yaml"
```

External links:

- The `Hlt1` decisions can be stored in similar way to `Hlt2` and `Spruce` (see example `option_trigger_decisions` in `DaVinciExamples` folder). For details, can also refer to the [talk](https://indico.cern.ch/event/1164051/#5-accessing-hlt1-decisions-usi) (The talk mentions that to persist Hlt1 decisions, one needs to add few options to the Moore script).
- List of currently available `functorcollections` are [here](https://gitlab.cern.ch/lhcb/Analysis/-/blob/master/Phys/FunTuple/python/FunTuple/functorcollections.py) and the planned ones are [here](https://gitlab.cern.ch/lhcb-dpa/project/-/issues/178).


## Tutorial5: MC truth association and background category algorithm

Objectives:

- Configuring the MC association and background category algorithm (`MCTruthAndBkgCatAlg`) to build a relation table. For MC association, the table is essentially a map between reconstructed particles and "truth" particles (MCParticle).
- Usage of the relations table and `ThOr` functor handling such table (e.g. `F.MAP_INPUT(func, RelTable`) to get truth information and background category.
- Also explore `functorcollections` such as `MCKinematics`, `MCHierarchy`, etc.

Command to run the tutorial:

```bash
lbexec DaVinciTutorials.tutorial5_MCTruth:main "$DAVINCITUTORIALSROOT/options.yaml"
```

## Tutorial6: Decay Tree Fitter (DTF) algorithm

Objectives:

- Configuring the Decay Tree Fitter algorithm (`DecayTreeFitterAlg`) to build a relation table i.e. map between the candidate and the refitted candidate.
- Usage of the relations table and `ThOr` functor (e.g. `F.MAP_INPUT(func, RelTable`) to get refitted information of the candidate.
- Defining different instances of DTF algorithm with mass constraints, primary vertex constraint.

Command to run the tutorial:

```bash
lbexec DaVinciTutorials.tutorial6_DecayTreeFit:main "$DAVINCITUTORIALSROOT/options.yaml"
```

External links on decay tree fitter:

- Paper: https://inspirehep.net/literature/679286
- Twiki : https://twiki.cern.ch/twiki/bin/view/LHCb/DecayTreeFitter
- Starter-kit: https://lhcb.github.io/starterkit-lessons/first-analysis-steps/decay-tree-fitter.html
- Slides by Wouter: https://www.nikhef.nl/~wouterh/topicallectures/TrackingAndVertexing/part6.pdf

## Tutorial7: Defining different instances of FunTuple when analysing outputs of multiple selection lines

Objectives:

- Defining different instances of FunTuple to return different `TDirectory` in the output ROOT file.
- Changes to the function returning user algorithm "sequence" to allow for this.

Command to run the tutorial:

```bash
lbexec DaVinciTutorials.tutorial7_multiple_sel_lines:main "$DAVINCITUTORIALSROOT/options.yaml"
```
