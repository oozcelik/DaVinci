###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test of a DST produced by HLT2 (Moore).
"""
from PyConf.Algorithms import PrintDecayTree
from RecoConf.reconstruction_objects import upfront_reconstruction
from DaVinci import Options, make_config
from DaVinci.common_particles import make_std_loose_d2kk


def d2kk(options: Options):
    d0s = make_std_loose_d2kk()
    pdt = PrintDecayTree(name="PrintD0s", Input=d0s)

    # the "upfront_reconstruction" is what unpacks reconstruction objects, particles and primary vertices
    # from file and creates protoparticles.
    algs = upfront_reconstruction() + [d0s, pdt]

    return make_config(options, algs)
