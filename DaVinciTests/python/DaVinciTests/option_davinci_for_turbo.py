###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test for checking the correct processing of Hlt2 .dst file where packed reco
objects are persisted by means of a pass through line.
"""
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple.functorcollections import Kinematics
from PyConf.reading import get_particles
from DaVinci import Options, make_config
from DaVinci.algorithms import add_filter


def main(options: Options):
    bs2jpsiphi_line = "Hlt2Generic_Bs0ToJpsiPhi_JPsiToMupMum_Line"
    bs2jpsiphi_data = get_particles(f"/Event/HLT2/{bs2jpsiphi_line}/Particles")
    fields = {
        'B0': "[B0 -> D_s- pi+]CC",
        'Ds': "[B0 -> ^D_s- pi+]CC",
        'pip': "[B0 -> D_s- ^pi+]CC",
    }

    #FunTuple: make functor collection from the imported functor library Kinematic
    variables_all = Kinematics()
    #FunTuple: associate functor collections to branch name
    variables = {
        'ALL': variables_all,  #adds variables to all branches
    }

    filter_bd = add_filter("HDRFilter_Bs2JpsiPhi",
                           f"HLT_PASS('{bs2jpsiphi_line}')")

    tuple_bd = Funtuple(
        name="B0DsPi_Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=bs2jpsiphi_data)

    algs = {
        "B0Dspi": [filter_bd, tuple_bd],
    }

    return make_config(options, algs)
