###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read the output of an Sprucing job with the new DaVinci configuration.
"""
from DaVinci.algorithms import add_filter
from DaVinci import Options, make_config


def main(options: Options):
    filter_B0DsK = add_filter(
        "HDRFilter_B0DsK",
        "HLT_PASS('SpruceB2OC_BdToDsmK_DsmToHHH_FEST_LineDecision')")
    filter_B0Dspi = add_filter(
        "HDRFilter_B0Dspi",
        "HLT_PASS('SpruceB2OC_BdToDsmPi_DsmToHHH_LineDecision')")

    algs = {"B0DsK": [filter_B0DsK], "B0Dspi": [filter_B0Dspi]}
    return make_config(options, algs)
