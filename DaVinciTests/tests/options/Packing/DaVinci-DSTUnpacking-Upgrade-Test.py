###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import DaVinci
from Configurables import TrackMonitor
from Configurables import CondDB
from PRConfig import TestFileDB

DaVinci().EvtMax = 100  # Number of events
DaVinci().PrintFreq = 10  # Events to skip
DaVinci().DataType = "Upgrade"  # Must be given
DaVinci().Simulation = True
DaVinci().InputType = 'LDST'
DaVinci().Lumi = False
CondDB().Upgrade = True
DaVinci().CondDBtag = "sim-20171127-vc-md100"
DaVinci().DDDBtag = "dddb-20171126"

DaVinci().HistogramFile = "dst-unpacking.root"

########################################################################

DaVinci().UserAlgorithms += [TrackMonitor()]

########################################################################

TestFileDB.test_file_db["Upgrade_Bd2KstarMuMu"].run()
