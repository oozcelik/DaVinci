###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read an HLT2 file and create an ntuple with information from RecSumarry
(e.g. nPVs, nTracks, nFTClusters)
"""
import Functors as F
from FunTuple import FunctorCollection as FC
from FunTuple import FunTuple_Particles as Funtuple
from DaVinci.algorithms import add_filter
from DaVinci import make_config, Options
from FunTuple.functorcollections import EventInfo
from PyConf.reading import get_particles, get_rec_summary


def main(options: Options):
    #define fields
    fields = {'Lb': '[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) mu-]CC'}

    #define variables and add them to all fields
    #lb_variables = Kinematics()
    lb_variables = FC({'PT': F.PT})
    variables = {"ALL": lb_variables}

    #get RecSummary object that holds information about nPVs, nTracks, nFTClusters
    # Note more information can be added to the RecSummary object
    # (see MRs: https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/1649)
    rec_summary = get_rec_summary()
    evt_vars = FC({
        'nTracks': F.VALUE_OR(-1) @ F.NTRACKS(rec_summary),
        'nPVs': F.VALUE_OR(-1) @ F.NPVS(rec_summary),
        'nFTClusters': F.VALUE_OR(-1) @ F.NFTCLUSTERS(rec_summary)
    })
    #Get RunNUmber and EventNumber
    evt_vars += EventInfo(extra_info=True)

    #get particles to run over
    line_name = 'Hlt2SLB_LbToLcMuNu_LcToPKPi_Line'
    particles = get_particles(f"/Event/HLT2/{line_name}/Particles")
    my_filter = add_filter("Myfilter", f"HLT_PASS('{line_name}')")
    #define tupling algorithm
    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        event_variables=evt_vars,
        inputs=particles)

    return make_config(options, [my_filter, my_tuple])
