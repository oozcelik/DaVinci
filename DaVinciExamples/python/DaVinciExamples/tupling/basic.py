###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of a typical DaVinci job:
 - reconstruction and selection of two detached muons
 - user algorithm printing decay trees via `PrintDecayTree`
 - tuple of the selected candidates
"""
import Functors as F

from Hlt2Conf.standard_particles import make_detached_mumu, make_KsDD
from RecoConf.reconstruction_objects import upfront_reconstruction
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple

from DaVinci import Options, make_config


def main(options: Options):
    # selections
    dimuons = make_detached_mumu()
    kshorts = make_KsDD()

    #FunTuple: make fields (branches) to tuple
    fields = {}
    fields['Jpsi'] = 'J/psi(1S) -> mu+ mu-'
    fields['MuPlus'] = 'J/psi(1S) -> ^mu+ mu-'

    #FunTuple: make collection of functors for Jpsi
    variables_jpsi = {
        'LOKI_P': 'P',
        'LOKI_PT': 'PT',
        'LOKI_Muonp_PT': 'CHILD(PT, 1)',
        'LOKI_Muonm_PT': 'CHILD(PT, 2)',
        'LOKI_MAXPT': 'TRACK_MAX_PT',
        'LOKI_N_HIGHPT_TRCKS': 'NINTREE(ISBASIC & HASTRACK & (PT > 1500*MeV))',
        'THOR_P': F.P,
        'THOR_PT': F.PT
    }

    #FunTuple: make collection of functors for Muplus
    variables_muplus = {'LOKI_P': 'P', 'THOR_P': F.P}

    #FunTuple: associate functor collections to field (branch) name
    variables = {}
    variables['Jpsi'] = FunctorCollection(variables_jpsi)
    variables['MuPlus'] = FunctorCollection(variables_muplus)

    #FunTuple: define list of preambles for loki
    loki_preamble = ['TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)']

    #FunTuple: Configure Funtuple algorithm
    tuple_dimuons = Funtuple(
        name="DimuonsTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        loki_preamble=loki_preamble,
        inputs=dimuons)

    #FunTuple: similarly for Ks we can make fields (branches), functor collections, variables and FunTuple instance
    fields_KS = {}
    fields_KS['KS'] = 'KS0 -> pi+ pi-'
    #associate the functor collections to KS field name (NB: here we use functor collection used for jpsi)
    variables_KS = {}
    variables_KS['KS'] = FunctorCollection(variables_jpsi)
    #funtuple instance
    tuple_kshorts = Funtuple(
        name="KsTuple",
        tuple_name="DecayTree",
        fields=fields_KS,
        variables=variables_KS,
        loki_preamble=loki_preamble,
        inputs=kshorts)

    algs = {
        'DiMuons': upfront_reconstruction() + [tuple_dimuons],
        'KShorts': upfront_reconstruction() + [tuple_kshorts]
    }

    return make_config(options, algs)
