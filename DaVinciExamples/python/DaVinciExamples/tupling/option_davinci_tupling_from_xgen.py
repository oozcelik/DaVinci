###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read and process a .xgen file with the new DaVinci configuration.
"""
from FunTuple import FunctorCollection, FunTuple_MCParticles as FuntupleMC
from PyConf.Algorithms import PrintMCTree
import Functors as F
from DaVinci import Options, make_config
from PyConf.reading import get_mc_particles


def main(options: Options):
    #FunTuple: define fields (branches)
    fields = {
        'Bu': "[B+ -> (J/psi(1S) -> mu+ mu-) K+]CC",
        'Jpsi': "[B+ -> ^(J/psi(1S) -> mu+ mu-) K+]CC",
        'Kplus': "[B+ -> (J/psi(1S) -> mu+ mu-) ^K+]CC",
        'muplus': "[B+ -> (J/psi(1S) -> ^mu+ mu-) K+]CC",
        'muminus': "[B+ -> (J/psi(1S) -> mu+ ^mu-) K+]CC",
    }

    #FunTuple: define variables for the B meson
    variables_B = FunctorCollection({
        'ETA': F.ETA,
        'PHI': F.PHI,
        'ORIGIN_VX': F.ORIGIN_VX,
        'ORIGIN_VY': F.ORIGIN_VY,
        'ORIGIN_VZ': F.ORIGIN_VZ,
        'END_VX': F.END_VX,
        'END_VY': F.END_VY,
        'END_VZ': F.END_VZ,
    })

    #FunTuple: define common variables
    variables_all = FunctorCollection({'PT': F.PT, 'P': F.FOURMOMENTUM})

    #FunTuple: associate functor collections to field (branch) name
    variables = {
        'ALL': variables_all,
        'Bu': variables_B,
    }

    bu2jpsik_line = get_mc_particles("/Event/MC/Particles")

    printMC = PrintMCTree(
        MCParticles=bu2jpsik_line, ParticleNames=["B+", "B-"], OutputLevel=4)

    tuple_BuJpsiK = FuntupleMC(
        name="BuJpsiK_MCTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=bu2jpsik_line)

    return make_config(options, {"BuJpsiK": [printMC, tuple_BuJpsiK]})
