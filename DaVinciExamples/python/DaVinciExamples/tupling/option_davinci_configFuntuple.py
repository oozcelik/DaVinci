###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example options to read the output of a Sprucing job with the new DaVinci configuration
filtering on an HLT2 line decision.
"""
from FunTuple import FunctorCollection
from FunTuple.functorcollections import Kinematics
from DaVinci.algorithms import configured_FunTuple
from DaVinci import Options, make_config


def main(options: Options):
    # FunTuple: define fields (branches)
    fields = {
        'B0': "[B0 -> D_s- pi+]CC",
        'Ds': "[B0 -> ^D_s- pi+]CC",
        'pip': "[B0 -> D_s- ^pi+]CC",
    }

    # FunTuple: define variables for the B meson
    variables_B = {
        'LOKI_MAXPT': 'TRACK_MAX_PT',
        'LOKI_Muonp_PT': 'CHILD(PT, 1)',
        'LOKI_Muonm_PT': 'CHILD(PT, 2)',
        'LOKI_NTRCKS_ABV_THRSHLD': 'NINTREE(ISBASIC & (PT > 15*MeV))'
    }

    # FunTuple: make functor collection from the imported functor library Kinematics
    variables_all = Kinematics()

    # FunTuple: associate functor collections to field (branch) name
    variables = {
        'ALL': variables_all,  # adds variables to all fields
        'B0': FunctorCollection(variables_B),
    }

    line = "SpruceB2OC_BdToDsmPi_DsmToKpKmPim_Line"
    config = {
        "location": f"/Event/Spruce/{line}/Particles",
        "filters": [f"HLT_PASS('{line}Decision')"],
        "preamble": ['TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)'],
        "tuple": "DecayTree",
        "fields": fields,
        "variables": variables,
    }

    algs = configured_FunTuple({"B0Dspi": config})

    return make_config(options, algs)
