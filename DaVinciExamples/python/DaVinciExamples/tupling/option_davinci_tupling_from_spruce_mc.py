###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from FunTuple import FunctorCollection
import Functors as F
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple.functorcollections import Kinematics
from DaVinci import Options, make_config
from DaVinci.truth_matching import configured_MCTruthAndBkgCatAlg
from DaVinci.algorithms import add_filter
from PyConf.reading import get_particles


def main(options: Options):
    #FunTuple: define branches.
    fields = {
        'B0': "[B0 -> D_s- K+]CC",
        'Ds': "[B0 -> ^D_s- K+]CC",
        'Kp': "[B0 -> D_s- ^K+]CC"
    }

    #FunTuple: make functor collection from the imported functor library Kinematics
    variables_all = Kinematics()

    #FunTuple: associate functor collections to branch name
    variables = {
        'ALL': variables_all,  #adds variables to all branches
    }

    line_name = 'Spruce_Test_line'
    B_data = get_particles(f"/Event/Spruce/{line_name}/Particles")

    my_filter = add_filter("HDRFilter_B", f"HLT_PASS('{line_name}')")

    #helper lambda function
    MCTRUTH = lambda func: F.MAP_INPUT(Functor=func, Relations=mctruth.MCAssocTable)

    #get configured "MCTruthAndBkgCatAlg" algorithm for HLT2 output
    mctruth = configured_MCTruthAndBkgCatAlg(inputs=B_data)
    #Add trueid info to each of the branches
    trueid_bkgcat_info = {
        # Important note: specify an invalid value for integer functors if there exists no truth info.
        #                 The invalid value for floating point functors is set to nan.
        "TRUEID": F.VALUE_OR(0) @ MCTRUTH(F.PARTICLE_ID),
        "TRUEKEY": F.VALUE_OR(-1) @ MCTRUTH(F.OBJECT_KEY),
        "TRUEPT": MCTRUTH(F.PT),
        "TRUEPX": MCTRUTH(F.PX),
        "TRUEPY": MCTRUTH(F.PY),
        "TRUEPZ": MCTRUTH(F.PZ),
        "TRUEENERGY": MCTRUTH(F.ENERGY),
        "TRUEP": MCTRUTH(F.P),
        "TRUEFOURMOMENTUM": MCTRUTH(F.FOURMOMENTUM),
        "BKGCAT": F.BKGCAT(Relations=mctruth.BkgCatTable),
    }
    for branch in variables.keys():
        variables[branch] += FunctorCollection(trueid_bkgcat_info)

    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=B_data)

    return make_config(options, [my_filter, mctruth, my_tuple])
