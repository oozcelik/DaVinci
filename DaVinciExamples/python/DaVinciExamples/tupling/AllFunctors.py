###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of a DaVinci job filling all available functors. This is obviously a stress test and not realistic.
"""
__author__ = "P. Koppenburg"
__date__ = "2021-11-23"

import Functors as F
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from PyConf.reading import get_particles, get_pvs
from DaVinci.algorithms import add_filter
from PyConf.reading import get_decreports, get_odin
from DecayTreeFitter import DecayTreeFitter
from DaVinci.truth_matching import configured_MCTruthAndBkgCatAlg
from PyConf.Algorithms import PrintDecayTree

from DaVinci import Options, make_config

#
# Definition of Sprucing line
#
bd2dsk_line = "SpruceB2OC_BdToDsmK_DsmToHHH_FEST_Line"

_basic = 'basic'
_composite = 'composite'
_toplevel = 'toplevel'


def all_variables(pvs, DTF, mctruth, ptype):
    """
    function that returns dictonary of functors that work.

    functors are listed in order of https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors_reference.html#module-Functors
    """
    if ptype not in [_basic, _composite]:
        Exception("I want {0} or {1}. Got {2}".format(_basic, _composite,
                                                      ptype))
    all_vars = {}

    comp = (_composite == ptype or _toplevel == ptype)  # is composite
    basic = (_basic == ptype)  # is not composite
    top = (_toplevel == ptype)  # the B

    # ALL : Not useful for tupling

    if comp:
        all_vars['ALV'] = F.ALV(Child1=1, Child2=2)

    all_vars['BKGCAT'] = F.BKGCAT(Relations=mctruth.BkgCatTable)

    if comp:  # all these require a vertex
        all_vars['BPVCORRM'] = F.BPVCORRM(pvs)
        all_vars['BPVDIRA'] = F.BPVDIRA(pvs)
        all_vars['BPVDLS'] = F.BPVDLS(pvs)
        all_vars['BPVETA'] = F.BPVETA(pvs)
        all_vars['BPVFD'] = F.BPVFD(pvs)
        all_vars['BPVFDCHI2'] = F.BPVFDCHI2(pvs)
        all_vars['BPVFDIR'] = F.BPVFDIR(pvs)
        all_vars['BPVFDVEC'] = F.BPVFDVEC(pvs)

    all_vars['BPVIP'] = F.BPVIP(pvs)
    all_vars['BPVIPCHI2'] = F.BPVIPCHI2(pvs)
    all_vars['BPVX'] = F.BPVX(pvs)
    all_vars['BPVY'] = F.BPVY(pvs)
    all_vars['BPVZ'] = F.BPVZ(pvs)
    # When storing variable length array one can
    # give a custom branch name for the index.
    # This can be achieved by enclosing custom index
    # name within square brackets (see code below).
    # The branch name ("nPV") will correspond to the
    # index of the PV. If no index branch name given i.e.
    # all_vars['ALLPVX'] the default "indx" is used.

    all_vars['ALLPVX[nPVs]'] = F.ALLPVX(pvs)
    all_vars['ALLPVY[nPVs]'] = F.ALLPVY(pvs)
    all_vars['ALLPVZ[nPVs]'] = F.ALLPVZ(pvs)

    if comp:  # all these require a vertex
        all_vars['ALLPV_FD[nPVs]'] = F.ALLPV_FD(pvs)
        all_vars['ALLPV_IP[nPVs]'] = F.ALLPV_IP(pvs)
        all_vars['BPVLTIME'] = F.BPVLTIME(pvs)
        all_vars['BPVVDRHO'] = F.BPVVDRHO(pvs)
        all_vars['BPVVDX'] = F.BPVVDX(pvs)
        all_vars['BPVVDY'] = F.BPVVDY(pvs)
        all_vars['BPVVDZ'] = F.BPVVDZ(pvs)

    all_vars['CHARGE'] = F.CHARGE
    all_vars['CHI2'] = F.CHI2
    all_vars['CHI2DOF'] = F.CHI2DOF
    if top:  # apply this only to B
        all_vars['CHILD1_PT'] = F.CHILD(1, F.PT)  # example of CHILD
        all_vars['Ds_END_VZ'] = F.CHILD(1, F.END_VZ)
        all_vars['Delta_END_VZ_DsB0'] = F.CHILD(1, F.END_VZ) - F.END_VZ

    # if basic: all_vars['CLOSESTTOBEAM'] = F.CLOSESTTOBEAM # 'Track__ClosestToBeamState' object has no attribute 'to_json'
    # COMB
    # if basic: all_vars['COV'] = F.COV # 'Track__Covariance' object has no attribute 'to_json'

    if comp:
        all_vars['DOCA'] = F.SDOCA(Child1=1, Child2=2)
        all_vars['DOCACHI2'] = F.SDOCACHI2(Child1=1, Child2=2)
        all_vars['END_VRHO'] = F.END_VRHO
        all_vars['END_VX'] = F.END_VX
        all_vars['END_VY'] = F.END_VY
        all_vars['END_VZ'] = F.END_VZ

    all_vars['ENERGY'] = F.ENERGY
    all_vars['ETA'] = F.ETA
    all_vars['FOURMOMENTUM'] = F.FOURMOMENTUM

    if basic:
        all_vars['GHOSTPROB'] = F.GHOSTPROB
        all_vars['ISMUON'] = F.ISMUON
        all_vars['INMUON'] = F.INMUON
        all_vars['INECAL'] = F.INECAL
        all_vars['INHCAL'] = F.INHCAL
        all_vars['HASBREM'] = F.HASBREM
        all_vars['BREMENERGY'] = F.BREMENERGY
        all_vars['BREMBENDCORR'] = F.BREMBENDCORR
        all_vars['BREMPIDE'] = F.BREMPIDE
        all_vars['ECALPIDE'] = F.ECALPIDE
        all_vars['ECALPIDMU'] = F.ECALPIDMU
        all_vars['HCALPIDE'] = F.HCALPIDE
        all_vars['HCALPIDMU'] = F.HCALPIDMU
        all_vars['ELECTRONSHOWEREOP'] = F.ELECTRONSHOWEREOP
        all_vars['CLUSTERMATCH'] = F.CLUSTERMATCH
        all_vars['ELECTRONMATCH'] = F.ELECTRONMATCH
        all_vars['BREMHYPOMATCH'] = F.BREMHYPOMATCH
        all_vars['ELECTRONENERGY'] = F.ELECTRONENERGY
        all_vars['BREMHYPOENERGY'] = F.BREMHYPOENERGY
        all_vars['BREMHYPODELTAX'] = F.BREMHYPODELTAX
        all_vars['ELECTRONINDEX'] = F.ELECTRONINDEX
        all_vars['HCALEOP'] = F.HCALEOP

    all_vars['IS_ABS_ID_pi'] = F.IS_ABS_ID('pi+')
    all_vars['IS_ID_pi'] = F.IS_ID('pi-')
    all_vars['IS_NOT_H'] = F.IS_NOT_H
    if basic:
        all_vars['IS_PHOTON'] = F.IS_PHOTON

    all_vars['DTF_PT'] = DTF.get_info(F.PT)
    all_vars['DTF_BPVIPCHI2'] = DTF.get_info(F.BPVIPCHI2(pvs))

    all_vars['MASS'] = F.MASS
    if top:  # B
        all_vars['MASSWITHHYPOTHESES'] = F.MASSWITHHYPOTHESES((939., 939.))
    elif comp:  # Ds
        all_vars['MASSWITHHYPOTHESES'] = F.MASSWITHHYPOTHESES((493.7, 493.7,
                                                               139.6))
    if comp:
        all_vars['MAXPT'] = F.MAX(F.PT)
        all_vars['MAXDOCA'] = F.MAXSDOCA
        all_vars['MAXDOCACHI2'] = F.MAXSDOCACHI2
        # the above in cut versions.

    # Important note: specify an invalid value for integer functors if there exists no truth info.
    #                 The invalid value for floating point functors is set to nan.
    all_vars['MC_MOTHER_ID'] = F.VALUE_OR(0) @ F.MAP_INPUT(
        Functor=F.MC_MOTHER(1, F.PARTICLE_ID), Relations=mctruth.MCAssocTable)

    if comp: all_vars['MINPT'] = F.MIN(F.PT)
    all_vars['MINIP'] = F.MINIP(pvs)
    all_vars['MINIPCHI2'] = F.MINIPCHI2(pvs)

    if basic:
        all_vars['TRACKPT'] = F.PT @ F.TRACK
        all_vars['TRACKHISTORY'] = F.VALUE_OR(-1) @ F.TRACKHISTORY @ F.TRACK
        all_vars['QOVERP'] = F.QOVERP @ F.TRACK
        all_vars['NDOF'] = F.VALUE_OR(-1) @ F.NDOF @ F.TRACK
        all_vars['NFTHITS'] = F.VALUE_OR(-1) @ F.NFTHITS @ F.TRACK
        all_vars['NHITS'] = F.VALUE_OR(-1) @ F.NHITS @ F.TRACK
        all_vars['NUTHITS'] = F.VALUE_OR(-1) @ F.NUTHITS @ F.TRACK
        all_vars['NVPHITS'] = F.VALUE_OR(-1) @ F.NVPHITS @ F.TRACK
        all_vars['TRACKHASVELO'] = F.VALUE_OR(-1) @ F.TRACKHASVELO @ F.TRACK
        all_vars['TRACKHASUT'] = F.VALUE_OR(-1) @ F.TRACKHASUT @ F.TRACK

    all_vars['OBJECT_KEY'] = F.OBJECT_KEY

    all_vars['ORIGIN_VX'] = F.MAP_INPUT(
        Functor=F.ORIGIN_VX, Relations=mctruth.MCAssocTable)
    all_vars['ORIGIN_VY'] = F.MAP_INPUT(
        Functor=F.ORIGIN_VY, Relations=mctruth.MCAssocTable)
    all_vars['ORIGIN_VZ'] = F.MAP_INPUT(
        Functor=F.ORIGIN_VZ, Relations=mctruth.MCAssocTable)

    all_vars['P'] = F.P
    all_vars['PARTICLE_ID'] = F.PARTICLE_ID
    all_vars['PHI'] = F.PHI
    #    all_vars['PHI_ADJ'] = F.ADJUST_ANGLE @ F.PHI

    if basic:
        all_vars['PID_E'] = F.PID_E
        all_vars['PID_K'] = F.PID_K
        all_vars['PID_MU'] = F.PID_MU
        all_vars['PID_P'] = F.PID_P
        all_vars['PID_PI'] = F.PID_PI
        # POD
        all_vars['PROBNN_D'] = F.PROBNN_D
        all_vars['PROBNN_E'] = F.PROBNN_E
        all_vars['PROBNN_GHOST'] = F.PROBNN_GHOST
        all_vars['PROBNN_K'] = F.PROBNN_K
        all_vars['PROBNN_MU'] = F.PROBNN_MU
        all_vars['PROBNN_P'] = F.PROBNN_P
        all_vars['PROBNN_PI'] = F.PROBNN_PI

    all_vars['PT'] = F.PT
    all_vars['PX'] = F.PX
    all_vars['PY'] = F.PY
    all_vars['PZ'] = F.PZ
    all_vars['ABS_PX'] = F.ABS @ F.PX

    all_vars['REFERENCEPOINT_X'] = F.REFERENCEPOINT_X
    all_vars['REFERENCEPOINT_Y'] = F.REFERENCEPOINT_Y
    all_vars['REFERENCEPOINT_Z'] = F.REFERENCEPOINT_Z

    if comp:
        all_vars['SDOCA'] = F.SDOCA(1, 2)
        all_vars['SDOCACHI2'] = F.SDOCACHI2(1, 2)
    if basic:
        all_vars['SHOWER_SHAPE'] = F.SHOWER_SHAPE

    if comp:
        all_vars['SUBCOMB12_MM'] = F.SUBCOMB(Functor=F.MASS, Indices=(1, 2))
        all_vars['SUMPT'] = F.SUM(F.PT)

    if basic:
        all_vars['TX'] = F.TX
        all_vars['TY'] = F.TY

    print("For {0} returning variables {1}".format(ptype, all_vars.keys()))
    return all_vars


def event_variables(PVs, ODIN, decreports):
    """
    event variables
    """
    evt_vars = {}
    if ODIN:
        evt_vars['BUNCHCROSSING_ID'] = F.BUNCHCROSSING_ID(ODIN)
        evt_vars['BUNCHCROSSING_TYPE'] = F.BUNCHCROSSING_TYPE(ODIN)
    if decreports:
        evt_vars['DECISIONS'] = F.DECISIONS(
            Lines=[bd2dsk_line + "Decision"], DecReports=decreports)
        evt_vars['DECREPORTS_FILTER'] = F.DECREPORTS_FILTER(
            Lines=[bd2dsk_line + "Decision"], DecReports=decreports)
    # DECREPORTS_RE_FILTER
    if ODIN:
        evt_vars['EVENTNUMBER'] = F.EVENTNUMBER(
            ODIN
        )  # WARNING FunTupleBase<Gaudi::NamedRange_<std::vector<LHCb::Particle const*,std::allocator<LHCb::Particle const*> >,__gnu_cxx::__normal_iterator<LHCb::Particle const* const*,std::vector<LHCb::Particle const*,std::allocator<LHCb::Particle const*> > > > >:: Tuple 'DecayTree' 'unsigned long' has different sizes on 32/64 bit systems. Casting 'EVENTNUMBER' to 'unsigned long long'
        evt_vars['EVENTTYPE'] = F.EVENTTYPE(ODIN)
        evt_vars['GPSTIME'] = F.GPSTIME(
            ODIN
        )  # WARNING FunTupleBase<Gaudi::NamedRange_<std::vector<LHCb::Particle const*,std::allocator<LHCb::Particle const*> >,__gnu_cxx::__normal_iterator<LHCb::Particle const* const*,std::vector<LHCb::Particle const*,std::allocator<LHCb::Particle const*> > > > >:: Tuple 'DecayTree' 'unsigned long' has different sizes on 32/64 bit systems. Casting 'EVENTNUMBER' to 'unsigned long long'
        evt_vars['ODINTCK'] = F.ODINTCK(ODIN)
        evt_vars['RUNNUMBER'] = F.RUNNUMBER(ODIN)
    evt_vars['PV_SIZE'] = F.SIZE(
        PVs)  # no matching function for call to 'invoke'
    if decreports: evt_vars['TCK'] = F.TCK(decreports)

    return evt_vars


def alg_config(options: Options):
    """
    Algorithm configuration function called from the comad line
    """
    #get the particles from line
    bd2dsk_data = get_particles(f"/Event/Spruce/{bd2dsk_line}/Particles")

    #
    # DecayTreeFitter Algorithm
    #
    v2_pvs = get_pvs()

    #
    # DecayTreeFitter Algorithm
    #
    DTF = DecayTreeFitter(name='DTF_Bd2DsK', input=bd2dsk_data)
    #
    # MC truth
    #
    mctruth = configured_MCTruthAndBkgCatAlg(inputs=bd2dsk_data)
    #
    # Definition of fields (branches) and functors
    #
    fields_dsk = {
        'B0': "[B0 -> (D_s- -> pi+ pi- pi-) K+]CC",
        'Kaon': "[B0 -> (D_s- -> pi+ pi- pi-) ^K+]CC",
        'Ds': "[B0 -> ^(D_s- -> pi+ pi- pi-) K+]CC",
        'pip': "[B0 -> (D_s- -> ^pi+ pi- pi-) K+]CC",
    }
    variables_dsk = {
        'B0': FunctorCollection(
            all_variables(v2_pvs, DTF, mctruth, _toplevel)),
        'Kaon': FunctorCollection(all_variables(v2_pvs, DTF, mctruth, _basic)),
        'Ds': FunctorCollection(
            all_variables(v2_pvs, DTF, mctruth, _composite)),
        'pip': FunctorCollection(all_variables(v2_pvs, DTF, mctruth, _basic)),
    }

    #
    # event variables
    #
    odin = None
    odin = get_odin()
    decreports = None
    decreports = get_decreports('Spruce')
    evt_vars = FunctorCollection(event_variables(v2_pvs, odin, decreports))

    #
    # Sprucing filter
    #
    my_filter = add_filter("HDRFilter_B0DsK", f"HLT_PASS('{bd2dsk_line}')")

    #
    # FunTuple
    #
    my_tuple = Funtuple(
        name="B0DsK_Tuple",
        tuple_name="DecayTree",
        fields=fields_dsk,
        variables=variables_dsk,
        event_variables=evt_vars,
        loki_preamble=[],
        inputs=bd2dsk_data)

    #
    # Algorithms to be run
    #
    return make_config(
        options, [PrintDecayTree(Input=bd2dsk_data), my_filter, my_tuple])
