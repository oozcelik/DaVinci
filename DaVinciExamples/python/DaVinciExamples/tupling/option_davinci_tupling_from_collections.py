###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read an HLT2 file and create an ntuple using pre-defined Functor collections.
"""

import Functors as F
from DaVinciMCTools import MCReconstructible, MCReconstructed
from FunTuple import FunctorCollection, functorcollections
from FunTuple import FunTuple_Particles as Funtuple, FunTuple_MCParticles as MCFuntuple
from DaVinci.algorithms import add_filter
from DaVinci import Options, make_config
from DaVinci.truth_matching import configured_MCTruthAndBkgCatAlg
from PyConf.Algorithms import WeightedRelTableAlg
from Gaudi.Configuration import INFO
from PyConf.reading import get_particles, get_mc_particles, get_mc_track_info


def main(options: Options):
    line_name = 'Hlt2Charm_D0ToKmPip_Line'

    d02kpi_data = get_particles(f"/Event/HLT2/{line_name}/Particles")
    MC_data = get_mc_particles("/Event/HLT2/MC/Particles")

    #Get variables related to reconstructible information.
    mcrtible = MCReconstructible(mc_track_info=get_mc_track_info())

    #Get variables related to reconstructed information.
    mcrted_all = MCReconstructed(MC_data, use_best_mcmatch=True)

    #get configured "MCTruthAndBkgCatAlg" algorithm for HLT2 output
    mctruth = configured_MCTruthAndBkgCatAlg(inputs=d02kpi_data)

    #configure "WeightedRelTableAlg" algorithm for HLT2 output
    iso_rel_table = WeightedRelTableAlg(
        ReferenceParticles=d02kpi_data,
        InputCandidates=d02kpi_data,
        Cut=(F.DR2() < 0.4),
        OutputLevel=INFO)

    # use functorcollections to add variables, please expand when there's new collections :)
    collections = [
        functorcollections.Kinematics(),
        functorcollections.MCHierarchy(mctruth),
        functorcollections.MCKinematics(mctruth),
        functorcollections.MCVertexInfo(mctruth),
        functorcollections.TrackIsolation(iso_rel_table),
    ]

    MC_collections = [
        functorcollections.MCReconstructible_Collection(
            mcrtible, extra_info=True),
        functorcollections.MCReconstructed_Collection(
            mcrted_all, extra_info=False),
        functorcollections.MCHierarchy(),
    ]

    evt_collections = [
        functorcollections.EventInfo(),
        functorcollections.SelectionInfo("Hlt2", [line_name])
    ]

    assert len(collections) + len(evt_collections) + len(
        MC_collections) - 1 == len(  # -1 because MCHierarchy is tested twice.
            functorcollections.__all__
        ), "Oh no! Did you forget to add a new collection to this test?"

    field_vars = FunctorCollection()
    for coll in collections:
        field_vars += coll

    evt_vars = FunctorCollection()
    for coll in evt_collections:
        evt_vars += coll

    MC_field_vars = FunctorCollection()
    for coll in MC_collections:
        MC_field_vars += coll

    fields = {
        "D0": "[D0 -> K- pi+]CC",
        "Kminus": "[D0 -> ^K- pi+]CC",
        "piplus": "[D0 -> K- ^pi+]CC",
    }

    #For now remove: The 'Hlt2' line decision tuples fine but breaks unit test with an error. (Why?)
    #see linked issue here: https://gitlab.cern.ch/lhcb/DaVinci/-/merge_requests/654#note_5320732
    print(evt_vars.pop(['Hlt2']))

    variables = {
        "D0": field_vars,
        "Kminus": field_vars,
        "piplus": field_vars,
    }

    MC_variables = {
        "D0": MC_field_vars,
        "Kminus": MC_field_vars,
        "piplus": MC_field_vars,
    }

    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        event_variables=evt_vars,
        inputs=d02kpi_data)

    my_filter = add_filter("HDRFilter_D0Kpi", f"HLT_PASS('{line_name}')")

    mc_tuple = MCFuntuple(
        name="MCTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=MC_variables,
        inputs=MC_data)

    return make_config(options, [mc_tuple, my_filter, my_tuple])
