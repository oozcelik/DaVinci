###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Option file for testing the ParticleWeightedAlg algorithm.
The job runs over a spruced sample and retrieves a set of B0 -> J/psi K candidates. For each candidate the algorithm
looks at the TES location which contains the tagged particles and creates a 'one-to-many' relation map
relating all the available tracks to the B candidate of the events.

Important: Setting DVPATH properly.
To run the example: $DVPATH/run lbexec option_davinci_tupling_weightedrelation_trackvariables:main $DVPATH/DaVinciExamples/example_data/spruce_b2jpsik_opt.yaml
"""

import Functors as F
from PyConf.Algorithms import WeightedRelTableAlg
from FunTuple import FunctorCollection, FunTuple_Particles as Funtuple
from FunTuple.functorcollections import TrackIsolation
from PyConf.reading import get_particles, get_pvs
from DaVinci.algorithms import add_filter
from DaVinci import Options, make_config


def main(options: Options):

    branches = {
        'B': "[B+ -> (J/psi(1S) -> mu+ mu- ) K+]CC",
        'Jpsi': "[B+ -> ^(J/psi(1S) -> mu+ mu- ) K+]CC",
        'Kp': "[B+ -> (J/psi(1S) -> mu+ mu- ) ^K+]CC"
    }

    b2jpsik_data = get_particles("/Event/HLT2/Hlt2B2JpsiKLine/Particles")
    tagged_data = get_particles(
        "/Event/HLT2/Hlt2B2JpsiKLine/LongTaggingParticles/Particles")

    pvs = get_pvs()

    ftAlg = WeightedRelTableAlg(
        ReferenceParticles=b2jpsik_data,
        InputCandidates=tagged_data,
        Cut=F.SHARE_BPV(pvs))

    ftAlg_Rels = ftAlg.OutputRelations
    #Set the variables
    extra_variables = FunctorCollection({
        'THOR_MASS':
        F.MASS,
        "First_P":
        F.MAP_INPUT(Functor=F.P, Relations=ftAlg_Rels),
        "First_PT":
        F.MAP_INPUT(Functor=F.PT, Relations=ftAlg_Rels),
        "Sum_P":
        F.SUMCONE(Functor=F.P, Relations=ftAlg_Rels),
        "Sum_PT":
        F.SUMCONE(Functor=F.PT, Relations=ftAlg_Rels),
        "Max_P":
        F.MAXCONE(Functor=F.P, Relations=ftAlg_Rels),
        "Max_PT":
        F.MAXCONE(Functor=F.PT, Relations=ftAlg_Rels),
        "Min_P":
        F.MINCONE(Functor=F.P, Relations=ftAlg_Rels),
        "Min_PT":
        F.MINCONE(Functor=F.PT, Relations=ftAlg_Rels),
        "Asym_P":
        F.ASYM(Functor=F.P, Relations=ftAlg_Rels),
        "Asym_PT":
        F.ASYM(Functor=F.PT, Relations=ftAlg_Rels),
        "Num_tracks":
        F.VALUE_OR(0) @ F.MAP_INPUT_SIZE(Relations=ftAlg_Rels),
    })

    variables_all = FunctorCollection({'THOR_P': F.P, 'THOR_PT': F.PT})

    track_iso_variables = TrackIsolation(ftAlg)

    variables_jpsik = {
        'B': variables_all + extra_variables,
        'Jpsi': variables_all,
        'Kp': variables_all + track_iso_variables,
    }

    my_filter = add_filter("HDRFilter_B2JpsiK",
                           "HLT_PASS('Hlt2B2JpsiKLineDecision')")
    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=branches,
        variables=variables_jpsik,
        inputs=b2jpsik_data)
    return make_config(options, [my_filter, my_tuple])
