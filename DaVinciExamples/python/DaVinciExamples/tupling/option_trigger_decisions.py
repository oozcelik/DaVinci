###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read an HLT2 file and create an ntuple with the new DaVinci configuration, accessing HLT1 and HLT2 decisions.
"""
import Functors as F
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from DaVinci import Options, make_config
from PyConf.reading import get_particles
from FunTuple.functorcollections import SelectionInfo


def main(options: Options):
    fields = {'B0': '[[B0]CC -> (D- -> K+ pi- pi-) pi+]CC'}

    B0_variables = FunctorCollection({"ID": F.PARTICLE_ID})

    variables = {"B0": B0_variables}

    b02dpi_data = get_particles(
        "/Event/HLT2/Hlt2B2OC_BdToDmPi_DmToPimPimKp_Line/Particles")

    Hlt1_decisions = [
        'Hlt1TrackMVADecision', 'Hlt1TwoTrackMVADecision', 'Hlt1D2KKDecision',
        'Hlt1D2KPiDecision', 'Hlt1D2PiPiDecision',
        'Hlt1DiMuonHighMassDecision', 'Hlt1DiMuonLowMassDecision',
        'Hlt1DiMuonSoftDecision', 'Hlt1GECPassthroughDecision',
        'Hlt1KsToPiPiDecision', 'Hlt1LowPtMuonDecision',
        'Hlt1LowPtDiMuonDecision', 'Hlt1SingleHighPtMuonDecision',
        'Hlt1TrackMuonMVADecision'
    ]

    #define event level variables
    evt_variables = SelectionInfo("Hlt1", Hlt1_decisions)

    #define FunTuple instance
    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        event_variables=evt_variables,
        inputs=b02dpi_data)

    return make_config(options, [my_tuple])
