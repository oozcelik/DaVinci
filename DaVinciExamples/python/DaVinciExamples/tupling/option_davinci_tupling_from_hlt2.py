###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read an HLT2 file and create an ntuple with the new DaVinci configuration.
"""
import Functors as F
from FunTuple import FunctorCollection as FC
from FunTuple import FunTuple_Particles as Funtuple
from PyConf.reading import get_particles, get_pvs, get_odin
from DaVinci.algorithms import add_filter
from DaVinci import Options, make_config
from DaVinci.truth_matching import configured_MCTruthAndBkgCatAlg
from FunTuple.functorcollections import SelectionInfo


def main(options: Options):
    fields = {
        "D0": "[D0 -> K- pi+]CC",
        "Kminus": "[D0 -> ^K- pi+]CC",
        "piplus": "[D0 -> K- ^pi+]CC",
    }

    # Creating v2 reconstructed vertices to be used in the following functor
    v2_pvs = get_pvs()

    d0_variables = FC({
        "ID": F.PARTICLE_ID,
        "KEY": F.OBJECT_KEY,
        "PT": F.PT,
        "PX": F.PX,
        "PY": F.PY,
        "PZ": F.PZ,
        "ENERGY": F.ENERGY,
        "P": F.P,
        "FOURMOMENTUM": F.FOURMOMENTUM,
        "BPVDIRA": F.BPVDIRA(v2_pvs),
        "BPVFDCHI2": F.BPVFDCHI2(v2_pvs),
        "BPVIPCHI2": F.BPVIPCHI2(v2_pvs)
    })

    daughter_variables = FC({
        "ID": F.PARTICLE_ID,
        "PT": F.PT,
        "PX": F.PX,
        "PY": F.PY,
        "PZ": F.PZ,
        "ENERGY": F.ENERGY,
        "P": F.P,
        "FOURMOMENTUM": F.FOURMOMENTUM,
    })

    variables = {
        "D0": d0_variables,
        "Kminus": daughter_variables,
        "piplus": daughter_variables
    }

    line_name = 'Hlt2Charm_D0ToKmPip_Line'
    d02kpi_data = get_particles(f"/Event/HLT2/{line_name}/Particles")

    my_filter = add_filter("HDRFilter_D0Kpi", f"HLT_PASS('{line_name}')")

    #get configured "MCTruthAndBkgCatAlg" algorithm for HLT2 output
    mctruth = configured_MCTruthAndBkgCatAlg(inputs=d02kpi_data)
    #add helper lambda that configures a functor to get truth information
    MCTRUTH = lambda func: F.MAP_INPUT(Functor=func, Relations=mctruth.MCAssocTable)
    trueid_bkgcat_info = {
        # Important note: specify an invalid value for integer functors if there exists no truth info.
        #                 The invalid value for floating point functors is set to nan.
        "TRUEID": F.VALUE_OR(0) @ MCTRUTH(F.PARTICLE_ID),
        "TRUEKEY": F.VALUE_OR(-1) @ MCTRUTH(F.OBJECT_KEY),
        "TRUEPT": MCTRUTH(F.PT),
        "TRUEPX": MCTRUTH(F.PX),
        "TRUEPY": MCTRUTH(F.PY),
        "TRUEPZ": MCTRUTH(F.PZ),
        "TRUEENERGY": MCTRUTH(F.ENERGY),
        "TRUEP": MCTRUTH(F.P),
        "TRUEFOURMOMENTUM": MCTRUTH(F.FOURMOMENTUM),
        "BKGCAT": F.BKGCAT(Relations=mctruth.BkgCatTable)
    }
    for field in variables.keys():
        variables[field] += FC(trueid_bkgcat_info)

    ##doesn't work since no "decayProducts" method in MCParticle
    #sort of related issue (https://gitlab.cern.ch/lhcb/Rec/-/issues/356)
    #variables['D0'] += FC({'TRUEPT_Kaon': MCTRUTH(F.CHILD(1, F.PT))})

    #get odin which hold the event information
    odin = get_odin()

    #define event level variables
    evt_variables = FC({
        "RUNNUMBER": F.RUNNUMBER(odin),
        "EVENTNUMBER": F.EVENTNUMBER(odin)
    })
    evt_variables += SelectionInfo("Hlt2", [line_name])
    #For now remove: The 'Hlt2' line decision tuples fine but breaks unit test with an error. (Why?)
    #see linked issue here: https://gitlab.cern.ch/lhcb/DaVinci/-/merge_requests/654#note_5320732
    evt_variables.pop('Hlt2')

    #define FunTuple instance
    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        event_variables=evt_variables,
        inputs=d02kpi_data)

    return make_config(options, [my_filter, my_tuple])
