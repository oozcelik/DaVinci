###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of a typical DaVinci job:
 - selection of two detached opposite-charge muons
 - tuple of the selected candidates
 - filter to select only the particles of interest
 - runs DecayTreeFitterAlg and stores some output
"""
import Functors as F
from Gaudi.Configuration import INFO
from DaVinci import Options, make_config
from DaVinci.algorithms import add_filter  #, filter_on
from DecayTreeFitter import DecayTreeFitter
from FunTuple import FunctorCollection as FC
from FunTuple import FunTuple_Particles as Funtuple
from PyConf.reading import get_particles


def main(options: Options):
    #fields for FunTuple
    fields = {}
    fields['B0'] = "[B0 -> D_s- K+]CC"
    fields['Ds'] = "[B0 -> ^D_s- K+]CC"
    fields['K'] = "[B0 -> D_s- ^K+]CC"

    #Get filtered particles (Note decay_descriptor is optional, if specified only B0 decays will be selected for processing)
    spruce_line = "SpruceB2OC_BdToDsmK_DsmToHHH_FEST_Line"
    # REPLACING TEMPORARY THE INPUT DATA
    #data_filtered = filter_on(
    #    f"/Event/Spruce/{spruce_line}/Particles",
    #    options.process,
    #    decay_descriptor=fields['B0'])
    data_filtered = get_particles(f"/Event/Spruce/{spruce_line}/Particles")

    # DecayTreeFitter Algorithm.
    DTF = DecayTreeFitter(
        name='DTF_filtered',
        input=data_filtered,
        mass_constraints=["D_s-"],
        output_level=INFO)

    #make collection of functors for all particles
    variables_all = FC({
        'THOR_P': F.P,
        'THOR_PT': F.PT,
        'THOR_MASS': F.MASS,
    })

    #make collection of functors for Ds meson
    variables_ds = FC({
        'DTF_PT':
        DTF.get_info(F.PT),
        'DTF_MASS':
        DTF.get_info(F.MASS),
        # Important note: specify an invalid value for integer functors if there exists no truth info.
        #                 The invalid value for floating point functors is set to nan.
        'DTF_CHILD1_ID':
        F.VALUE_OR(0) @ DTF.get_info(F.CHILD(1, F.PARTICLE_ID)),
        'DTF_CHILD1_MASS':
        DTF.get_info(F.CHILD(1, F.MASS)),
    })

    #associate FunctorCollection to field (branch) name
    variables = {'ALL': variables_all, 'Ds': variables_ds}

    filter_data = add_filter("SpruceFilter", f"HLT_PASS('{spruce_line}')")

    #Configure Funtuple algorithm
    tuple_data = Funtuple(
        name="B0DsK_Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=data_filtered)

    return make_config(options, [filter_data, tuple_data])
