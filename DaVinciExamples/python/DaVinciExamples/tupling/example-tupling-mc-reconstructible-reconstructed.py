###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example options to show the usage of the new DaVinciMCTools: MCReconstructible
and MCReconstructed.
"""

from FunTuple import FunTuple_MCParticles as MCFuntuple
from DaVinciMCTools import MCReconstructible, MCReconstructed
from DaVinci import Options, make_config
from FunTuple.functorcollections import MCReconstructed_Collection, MCReconstructible_Collection
from PyConf.reading import get_mc_particles, get_pp2mcp_relations, get_charged_protoparticles, get_neutral_protoparticles, get_mc_track_info


def main(options: Options):

    # Input
    MC_data = get_mc_particles("/Event/HLT2/MC/Particles")
    # PP2MCP relations need MC particles and ProtoParticles
    # Since MC is already unpacked, only unpack protos and relations
    extra_inputs = []
    extra_inputs += [get_charged_protoparticles()]
    extra_inputs += [get_neutral_protoparticles()]

    relations_charged = get_pp2mcp_relations(
        "/Event/HLT2/Relations/ChargedPP2MCP", extra_inputs=extra_inputs)
    relations_neutral = get_pp2mcp_relations(
        "/Event/HLT2/Relations/NeutralPP2MCP", extra_inputs=extra_inputs)

    #Get variables related to reconstructible information.
    mcrtible = MCReconstructible(mc_track_info=get_mc_track_info())
    #The option extra_info is set to False by default and can be set to True to get more information
    vars_rtible = MCReconstructible_Collection(mcrtible, extra_info=True)
    print('Reconstructible functors:', vars_rtible.functor_dict.keys())
    #Note instead of importing functorcollections (MCReconstructed_Collection), one
    # can directly add track related information using the helper class (MCReconstructible)
    vars_rtible["EXTRA_MC_HASUT"] = mcrtible.HasUT
    vars_rtible["EXTRA_MC_HASVELO"] = mcrtible.HasVelo
    print('Reconstructible functors:', vars_rtible.functor_dict.keys())

    #Get variables related to reconstructed information.
    # - If "use_best_mcmatch = True" (default), the best associated reconstructed
    #   track to the mc particle is used (tupling scalars).
    # - If "use_best_mcmatch = False", all associated reconstructed
    #   tracks to the mc particle are used (tupling arrays).
    #   Here we set it to false for testing purposes.
    mcrted_all = MCReconstructed(
        MC_data,
        use_best_mcmatch=False,
        relations_charged=relations_charged,
        relations_neutral=relations_neutral)
    #The option extra_info below is set to False by default in the functor collection.
    vars_rted = MCReconstructed_Collection(mcrted_all, extra_info=False)
    #Note:
    # - Instead of importing functorcollections (MCReconstructed_Collection), one
    #   can directly add track related information using the helper class (MCReconstructed).
    # - A new functor can be added for e.g. vars_rtible["MC_HASUT"] =  mcrtible.get_info(F.NEW_TRACK_FUNCTOR)
    vars_rted['TRACK_TYPE[TRACK_INDX]'] = mcrted_all.TrackType
    vars_rted['TRACK_HASUT[TRACK_INDX]'] = mcrted_all.HasUT
    print('Reconstructed functors:', vars_rted.functor_dict.keys())

    # Variables
    variables = {
        'ALL': vars_rted,
        # Apply reconstructible to charged particles
        'Kplus': vars_rtible,
        'pip': vars_rtible,
        'pim1': vars_rtible,
        'pim2': vars_rtible,
    }

    # Define fields
    fields = {
        'B0': '[[B0]CC -> (D- -> K+ pi- pi-) pi+]CC',
        'Dm': '[[B0]CC -> ^(D- -> K+ pi- pi-) pi+]CC',
        'Kplus': '[[B0]CC -> (D- -> ^K+ pi- pi-) pi+]CC',
        'pip': '[[B0]CC -> (D- -> K+ pi- pi-) ^pi+]CC',
        'pim1': '[[B0]CC -> (D- -> K+ ^pi- pi-) pi+]CC',
        'pim2': '[[B0]CC -> (D- -> K+ pi- ^pi-) pi+]CC'
    }

    # Make tuple algorithm
    tuple_Dpi = MCFuntuple(
        name="DpiMC",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=MC_data)
    # Run
    return make_config(options, [tuple_Dpi])
