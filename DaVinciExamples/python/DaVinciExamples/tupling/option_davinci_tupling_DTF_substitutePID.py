###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""option_davinci_tupling_DTF_substitutePID.py
Example options to show the usage of the new DaVinciTools: DecayTreeFitter.
"""

from Gaudi.Configuration import INFO
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import FunctorCollection as FC
from DaVinci.algorithms import add_filter
from PyConf.reading import get_particles, get_pvs_v1
from DecayTreeFitter import DecayTreeFitter

import Functors as F
from DaVinci import Options, make_config


def main(options: Options):

    B_Line = "Hlt2BsToJpsiPhi_JPsi2MuMu_PhiToKK_Line"
    B_Data = get_particles(f'/Event/HLT2/{B_Line}/Particles')

    my_filter = add_filter("HDRFilter_Bs2JpsiPhi", f"HLT_PASS('{B_Line}')")

    pvs_v1 = get_pvs_v1()
    DTF_JpsiPhi = DecayTreeFitter(
        name='DTF_JpsiPhi',
        input=B_Data,
        mass_constraints=['B_s0', 'J/psi(1S)'],
        input_pvs=pvs_v1,
        output_level=INFO)

    DTF_JpsiKst = DecayTreeFitter(
        name='DTF_JpsiKst',
        input=B_Data,
        substitutions=[
            'B_s0{{B0}}   -> (J/psi(1S) -> mu+ mu-) (phi(1020){{K*(892)0}}  -> K+  K-{{pi-}})',
            'B_s~0{{B0}}  -> (J/psi(1S) -> mu+ mu-) (phi(1020){{K*(892)~0}} -> K-  K+{{pi+}})',
        ],
        mass_constraints=['B0', 'J/psi(1S)'],
        input_pvs=pvs_v1,
        output_level=INFO)

    fields = {
        'Bs': "[ B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K-) ]CC",
        'Jpsi': "[ B_s0 -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K-) ]CC",
        'Phi': "[ B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) -> K+ K-) ]CC",
        'MuP': "[ B_s0 -> (J/psi(1S) -> ^mu+ mu-) (phi(1020) -> K+ K-) ]CC",
        'MuM': "[ B_s0 -> (J/psi(1S) -> mu+ ^mu-) (phi(1020) -> K+ K-) ]CC",
        'KP': "[ B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> ^K+ K-) ]CC",
        'KM': "[ B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ ^K-) ]CC",
    }

    variables_all = FC({
        # Original particle
        'ORIGINAL_ID':
        F.PARTICLE_ID,
        'ORIGINAL_M':
        F.MASS,
        'ORIGINAL_P':
        F.P,
        'ORIGINAL_ENERGY':
        F.ENERGY,
        'ORIGINAL_CHI2DOF':
        F.CHI2DOF,
        # DTF Bs2JpsiPhi
        'DTF_JpsiPhi_ID':
        F.VALUE_OR(-1) @ DTF_JpsiPhi.get_info(F.PARTICLE_ID),
        'DTF_JpsiPhi_M':
        DTF_JpsiPhi.get_info(F.MASS),
        'DTF_JpsiPhi_P':
        DTF_JpsiPhi.get_info(F.P),
        'DTF_JpsiPhi_ENERGY':
        DTF_JpsiPhi.get_info(F.ENERGY),
        'DTF_JpsiPhi_CHI2DOF':
        DTF_JpsiPhi.get_info(F.CHI2DOF),
        # DTF Bd2JpsiKst
        'DTF_JpsiKst_ID':
        F.VALUE_OR(-1) @ DTF_JpsiKst.get_info(F.PARTICLE_ID),
        'DTF_JpsiKst_M':
        DTF_JpsiKst.get_info(F.MASS),
        'DTF_JpsiKst_P':
        DTF_JpsiKst.get_info(F.P),
        'DTF_JpsiKst_ENERGY':
        DTF_JpsiKst.get_info(F.ENERGY),
        'DTF_JpsiKst_CHI2DOF':
        DTF_JpsiKst.get_info(F.CHI2DOF),
    })

    variables = {'ALL': variables_all}

    #Configure Funtuple algorithm
    tuple_data = Funtuple(
        name="Bs2JpsiPhi_Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=B_Data)

    # Run
    algs = {
        "Bs2JpsiPhi": [my_filter, tuple_data],
    }
    return make_config(options, algs)
