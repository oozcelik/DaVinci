<?xml version="1.0" ?>
<!--
###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->
<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>lbexec</text></argument>
  <argument name="args"><set>
    <text>DaVinciExamples.option_davinci_v2_composites:main</text>
  </set></argument>
  <argument name="options_yaml_fn"><text>../../example_data/FEST_November_2021_dst_newPacking.yaml</text></argument>
  <argument name="extra_options_yaml"><text>
    evt_max: 2
  </text></argument>
  <argument name="reference"><text>../refs/test_davinci_v2_composites.ref</text></argument>
  <argument name="error_reference"><text>../refs/empty.ref</text></argument>
  <argument name="validator"><text>
#from DaVinciExamples.QMTest.DaVinciExclusions import preprocessor, counter_preprocessor
#validateWithReference(preproc = preprocessor, counter_preproc = counter_preprocessor)
import sys, os, glob
from ROOT import TFile

B_vars_stored =['B_PX', 'B_PY', 'B_PZ']
K_vars_stored =['K_PX', 'K_PY', 'K_PZ']

#sort the expected vars
B_vars_stored = sorted(B_vars_stored)

#open the TFile and TTree
ntuple = './v2_example.root'
if not os.path.isfile(ntuple): raise Exception(f"File: {ntuple} does not exist!")
f      = TFile.Open(ntuple)
t_B    = f.Get('TupleB/DecayTree')
t_K    = f.Get('TupleK/DecayTree')

#sort the stores vars
b_names = sorted([b.GetName() for b in t_B.GetListOfLeaves()])
K_names = sorted([K.GetName() for K in t_K.GetListOfLeaves()])

B_excluded_1 = set(B_vars_stored) - set(b_names)
B_excluded_2 = set(b_names) - set(B_vars_stored)
if len(B_excluded_1) != 0: raise Exception('Number of stored variables is less than what is expected. The extra variables expected are: ' , B_excluded_1)
if len(B_excluded_2) != 0: raise Exception('Number of stored variables is greater than what is expected. The extra variables stored are: ', B_excluded_2)

K_excluded_1 = set(K_vars_stored) - set(K_names)
K_excluded_2 = set(K_names) - set(K_vars_stored)
if len(K_excluded_1) != 0: raise Exception('Number of stored variables is less than what is expected. The extra variables expected are: ' , K_excluded_1)
if len(K_excluded_2) != 0: raise Exception('Number of stored variables is greater than what is expected. The extra variables stored are: ', K_excluded_2)

f.Close()
print('Test successfully completed!')
os.system(f"rm {ntuple}")
#countErrorLines({"FATAL":0, "ERROR":0})
  </text></argument>
</extension>
