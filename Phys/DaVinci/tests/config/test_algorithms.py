###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import Gaudi__Examples__VoidConsumer as VoidConsumer

from DaVinci import Options
from DaVinci.algorithms import (
    define_fsr_writer,
    add_filter,  #filter_on
    apply_filters_and_unpacking,
    configured_FunTuple)
from PyConf.reading import get_odin, get_decreports, get_hlt_reports, upfront_decoder
from PyConf.application import default_raw_event


def test_define_write_fsr():
    """
    Check if DaVinci imports correctly the algorithm to merge and write FSRs.
    """
    options = Options(
        data_type="Upgrade",
        input_process="Turbo",
        evt_max=1,
        output_level=3,
        merge_genfsr=True,
        simulation=True,
    )
    test_algs = define_fsr_writer(options)
    assert any("GenFSRMerge" == x.name for x in test_algs)


def test_add_hlt2_filter():
    """
    Check if DaVinci is able to implement correctly a filter on an HLT2 line.
    """
    options = Options(
        data_type="Upgrade",
        input_raw_format=0.5,
        evt_max=1,
        simulation=True,
        input_process="Hlt2",
        stream="default",
    )
    #Note here that we need to manually apply a bind to the PyConf functions
    # as they are not automatically configured in the pytests.
    # When running DV, the PyConf functions are globally configured and one must avoid
    # "binding" as much as possible.
    with default_raw_event.bind(raw_event_format=options.input_raw_format),\
             get_hlt_reports.bind(input_process=options.input_process, stream=options.stream):
        test_filter = add_filter("test_filter",
                                 "HLT_PASS('Hlt2TESTLineDecision')")
    assert "HDRFilter" in test_filter.fullname


def test_add_spruce_filter():
    """
    Check if DaVinci is able to implement correctly a filter on a Sprucing line.
    """
    options = Options(
        data_type="Upgrade",
        input_raw_format=0.5,
        evt_max=1,
        simulation=True,
        input_process="Spruce",
        stream="default",
    )
    with default_raw_event.bind(raw_event_format=options.input_raw_format),\
             get_hlt_reports.bind(input_process=options.input_process, stream=options.stream):
        test_filter = add_filter("test_filter",
                                 "HLT_PASS('SpruceTESTLineDecision')")
    assert "HDRFilter" in test_filter.fullname


def test_add_void_filter():
    """
    Check if DaVinci is able to implement correcty a Void filter
    if 'HLT_PASS' string is not found in the filter code."
    """
    options = Options(
        data_type="Upgrade",
        input_process="Turbo",
        evt_max=1,
        simulation=True,
    )
    with get_hlt_reports.bind(
            input_process=options.input_process, stream=options.stream):
        test_filter = add_filter("test_filter", "VOIDTEST_Filter")
    assert "VoidFilter" in test_filter.fullname


def test_apply_filters_and_unpack():
    """
    Check if DaVinci applies correctly a filter in front of a given algorithm
    """
    options = Options(
        data_type="Upgrade",
        input_raw_format=0.5,
        input_process="Turbo",
        evt_max=1,
        evt_pre_filters={"test_filter": "EVT_PREFILTER"},
        simulation=True,
    )
    alg_dict = {"test_alg": [VoidConsumer()]}
    test_alg_dict = apply_filters_and_unpacking(options, alg_dict)
    list_of_main_expected_algs = ["LoKi__VoidFilter"]

    for exp_alg in list_of_main_expected_algs:
        assert any(
            exp_alg in alg.fullname for alg in test_alg_dict["test_alg"])


def test_configured_funtuple():
    """
    Check if the configured_FunTuple provides a correct instance of FunTuple.
    """
    from FunTuple.functorcollections import Kinematics
    fields = {'B0': "[B0 -> D_s- pi+]CC"}
    variables = {'B0': Kinematics()}
    config = {
        "TestTuple": {
            "location": "/Event/Spruce/SpruceTestLine/Particles",
            "filters":
            ["HLT_PASS('SpruceTestLine1Decision')", "VoidTest_Filter"],
            "preamble": ['TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)'],
            "tuple": "DecayTree",
            "fields": fields,
            "variables": variables,
        }
    }
    options = Options(
        data_type="Upgrade",
        input_process="Turbo",
        evt_max=1,
        input_raw_format=0.5,
        simulation=True,
    )
    with default_raw_event.bind(raw_event_format=options.input_raw_format),\
             upfront_decoder.bind(input_process=options.input_process, stream=options.stream),\
             get_hlt_reports.bind(input_process=options.input_process, stream=options.stream):
        test_dict = configured_FunTuple(config)
    assert any("FunTupleBase_Particles/Tuple_TestTuple" in alg.fullname
               for alg in test_dict["TestTuple"])


def test_get_odin():
    """
    Check if get_odin provides a correct instance of ODIN.
    """
    options = Options(
        data_type="Upgrade",
        input_process="Turbo",
        evt_max=1,
        input_raw_format=0.5,
        simulation=True,
    )
    with default_raw_event.bind(raw_event_format=options.input_raw_format):
        odin = get_odin(
            input_process=options.input_process, stream=options.stream)
    assert odin.location == "/Event/createODIN#1/ODIN"


def test_get_decreports():
    """
    Check if get_decreports provide a correct instance of HltDecReportsDecoder.
    """
    options = Options(
        data_type="Upgrade",
        input_raw_format=0.5,
        evt_max=1,
        simulation=True,
        input_process="Turbo",
        stream="TurboSP",
    )
    with default_raw_event.bind(raw_event_format=options.input_raw_format),\
             get_hlt_reports.bind(input_process=options.input_process, stream=options.stream):
        decreports = get_decreports("Hlt2")
    assert decreports.location == "/Event/Hlt2/DecReports"


"""
def test_filter_on_and_apply_algorithms():
    ""
    Check if filter_on and apply_algorithms functions return a correct filtered particle location."
    ""
    spruce_line = "SpruceB2OC_BdToDsmK_DsmToHHH_FEST_Line"
    decay_descriptor = "[B0 -> D_s- K+]CC"
    data_filtered = filter_on(f"/Event/Spruce/{spruce_line}/Particles",
                              decay_descriptor)
    assert data_filtered.location == "/Event/FilterDecays/particles"
"""
