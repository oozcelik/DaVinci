###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import INFO

from PyConf.Algorithms import MCTruthAndBkgCatAlg
from PyConf.Tools import DaVinciSmartAssociator, MCMatchObjP2MCRelator, ParticleDescendants
from PyConf.Tools import BackgroundCategory, BackgroundCategoryViaRelations
from PyConf.Tools import P2MCPFromProtoP
from PyConf.reading import get_pp2mcp_relations, get_mc_particles, get_charged_protoparticles, get_neutral_protoparticles, get_tes_root
from PyConf.location_prefix import prefix


def configured_MCTruthAndBkgCatAlg(
        inputs,
        relations_locs=["Relations/ChargedPP2MCP", "Relations/NeutralPP2MCP"],
        root_in_tes=None,
        redo_neutral_assoc=False,
        filter_MCP=True,
        output_level=INFO):
    """
    Function to help configure the tools instantiated by the `MCTruthAndBkgCatAlg` algorithm.

    The tools configured are `DaVinciSmartAssociator`, `MCMatchObjP2MCRelator`,
    `BackgroundCategory`, `BackgroundCategoryViaRelations` and `P2MCPFromProtoP`.

    The configuration for Sprucing output is the same as for HLT2 output
    provided the correct `relations_locs` and `root_in_tes` arguments are set.

    The output of this algorithm is two relation tables:
    one for MC association (P->MCP) and one for background category (P->BKGCAT).
    The MC association table can be used as input to the `MAP_INPUT` functor
    and the background category table serves as input for `BKGCAT` functor.

    Args:
        inputs (DataHandle): Output of `Gaudi::Hive::FetchDataFromFile` (the input TES location to the particles).
        relations_locs (list, optional): TES locations to the pre-existing relations for charged and neutral particles.
            Defaults to ["Relations/ChargedPP2MCP", "Relations/NeutralPP2MCP"].
        root_in_tes (str, optional): RootInTES location that can be different for streamed output. Defaults is None.
            If not 'None' then it is inferred from the 'input_process' option in yaml file.
        redo_neutral_assoc (bool, optional): Whether or not to redo MC association of pure neutral calorimetric basic particle,
            i.e. gamma and pi0-merged with pi0-resolved treated as composite.
            Defaults to False.
        output_level (int, optional): the standard `OutputLevel` from `Gaudi.Configuration`
            to set for all instantiated algorithms and tools. Defaults to `output_level=INFO=3`.

    Returns:
        MCTruthAndBkgCatAlg: configured instance of algorithm MCTruthAndBkgCatAlg.
    """

    if not root_in_tes:
        root_in_tes = get_tes_root()

    # No algorithm is asking for mc particles and proto particles explicitely
    # But pp2mcp relation unpacker needs them, so give them as extra_inputs
    extra_inputs = []
    extra_inputs += [get_charged_protoparticles()]
    extra_inputs += [get_neutral_protoparticles()]

    mc_loc = root_in_tes + "/MC/Particles"
    extra_inputs += [get_mc_particles(mc_loc)]

    relations = [
        get_pp2mcp_relations(prefix(rel, root_in_tes), extra_inputs)
        for rel in relations_locs
    ]

    # Tool used by DaVinciSmartAssociator
    p2mctool = P2MCPFromProtoP(
        Locations=relations, RootInTES=root_in_tes, OutputLevel=output_level)

    # Tools used by MCTruthAndBkgCatAlg
    bkg_cat = BackgroundCategory(
        P2MCTool=p2mctool,
        ExtraInputs=relations,
        vetoNeutralRedo=not redo_neutral_assoc,
        RootInTES=root_in_tes,
        OutputLevel=output_level)
    bkg_cat_via_rel = BackgroundCategoryViaRelations(
        RootInTES=root_in_tes, OutputLevel=output_level)
    dv_assc = DaVinciSmartAssociator(
        P2MCTool=p2mctool,
        RedoNeutral=redo_neutral_assoc,
        RootInTES=root_in_tes,
        BackgroundCategoryTool=bkg_cat,
        OutputLevel=output_level)
    mcrel_assc = MCMatchObjP2MCRelator(
        RelTableLocations=relations,
        #Setting RootInTES for this tool gives a segfaul (why?), we anyway use DaVinciSmartAssociator for association.
        #RootInTES=root_in_tes,
        OutputLevel=output_level)
    part_desc = ParticleDescendants(
        RootInTES=root_in_tes, OutputLevel=output_level)

    mctruth = MCTruthAndBkgCatAlg(
        Input=inputs,
        filter_MCP=filter_MCP,
        DaVinciSmartAssociator=dv_assc,
        MCMatchObjP2MCRelator=mcrel_assc,
        BackgroundCategory=bkg_cat,
        BackgroundCategoryViaRelations=bkg_cat_via_rel,
        ParticleDescendants=part_desc,
        OutputLevel=output_level)

    return mctruth
