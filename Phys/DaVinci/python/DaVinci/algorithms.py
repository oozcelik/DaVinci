###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
import logging

from PyConf.Algorithms import (
    FilterDecays,
    LoKi__HDRFilter as HDRFilter,
    LoKi__VoidFilter as VoidFilter,
)
from Hlt2Conf.algorithms import make_dvalgorithm
from PyConf.reading import get_particles, get_hlt_reports

log = logging.getLogger(__name__)


def set_filter(name, code, dec_reports):
    """
    Adding an HDR filter to FunTuple algorithms checking if the corresponding HLT/Sprucing line fired.

    Args:
        name (str): filter's name.
        code (str): filter's code.
        dec_reports (HltDecreportsDecoder instance): HltDecReportsDecoder containing the configuration for HLT/Sprucing lines.

    Returns:
        Filter with name and code defined by the user.
    """
    if dec_reports:
        algFilter = HDRFilter(
            name=name,
            Code=code,
            Location=dec_reports.OutputHltDecReportsLocation)
    else:
        algFilter = VoidFilter(name=name, Code=code)

    return algFilter


def add_filter(name, code):
    """
    Adding an event pre-filter using a code defined by the user.

    Args:
        name (str): filter's name.
        code (str): filter's code.

    Returns:
        Filter with name and code defined by the user.
    """
    #if code ends with HLT_PASS then check that the lines inside
    # HLT_PASS contain suffix decision
    if code.startswith("HLT_PASS("):
        #check if it ends with braces
        if not code.endswith(")"):
            raise Exception("The specified code must end with brackets ')'")

        #find all the line names between apostrophe
        # e.g. turns string "HLT_PASS('Line1 ','Line2')" to list('Line1', 'Line2)
        line_names = re.findall(r"\'([^\']*)\'", code)

        #remove whitespace
        remove_whitespace = lambda s: s.replace(' ', '')
        line_names = list(map(remove_whitespace, line_names))

        #check for suffix 'Decision', if not add one
        add_suff_to_elem = lambda s: s + 'Decision' if not s.endswith('Decision') else s
        line_names = list(map(add_suff_to_elem, line_names))

        #put back the code together
        code = 'HLT_PASS(\'' + '\',\''.join(line_names) + '\')'

    dec_reports = None
    for source in ["Hlt2", "Spruce"]:
        if re.search("^HLT_PASS.*" + source, code):
            dec_reports = get_hlt_reports(source)
            break

    algFilter = set_filter(name, code, dec_reports)
    return algFilter


def apply_filters_and_unpacking(options, algs_dict):
    """
    Adding filter and unpacking algorithms.

    Args:
        options (DaVinci.Options): lbexec provided options object
        algs_dict (dict): dict of the user algorithms.
        (TO BE REMOVED WHEN THE UNPACKING WILL BECOME FUNCTIONAL)
        (UNPACKING IS FUNCTIONAL, time to remove maybe?)

    Returns:
        Dict where at each node filters and unpacking algorithms are prepended to the initial list of user algorithms.
    """

    alg_filterd_dict = {}
    for name, algs in algs_dict.items():
        algs_list = []
        if options.evt_pre_filters:
            evt_pre_filters = []
            for title, code in options.evt_pre_filters.items():
                evt_filter = add_filter(title, code)
                evt_pre_filters.append(evt_filter)
            algs_list += evt_pre_filters

        algs_list += algs

        alg_filterd_dict[name] = algs_list

    return alg_filterd_dict


def define_fsr_writer(options):
    """
    Define Generator FSR writer.

    Args:
        options (DaVinci.Options): lbexec provided options object

    Returns:
        List of FSR algorithm instances to be configured.
    """
    from PyConf.Algorithms import GenFSRMerge, RecordStream

    algs = []
    if options.merge_genfsr and options.simulation:
        mergeGenfsr = GenFSRMerge(name="GenFSRMerge")
        algs.append(mergeGenfsr)

    outputLevel = options.output_level
    recStream = RecordStream(
        name="FSROutputStreamDstWriter",
        OutputLevel=outputLevel,
        Output="SVC='Gaudi::RootCnvSvc'")
    algs.append(recStream)

    return algs


def configured_FunTuple(config):
    """
    Function for the FunTuple configuration and instantiation of the related HDR filter.

    Args:
        config (dict): configuration dict containing information for the tuple setting:
          - dict key: name of FunTuple algorithm to be instantiated.
          - tuple: name of the ntuple built by Funtuple.
          - fields: dict containing all the fields (branches) to be stored in Funtuple.
          - variables: dict containing FunctorCollection to be associated to the Funtuple fields (branches).
          - preamble: string for the LoKi preamble
          - location: string containing the particles location to be used as input.
          - filters: list of line decisions for filtering the events.

    Returns:
       - List of filters and tupling algorithms.
    """
    from FunTuple import FunTuple_Particles as Funtuple

    dictAlgs = {}
    for key in config.keys():
        inputs = get_particles(config[key]["location"])
        dictAlgs[key] = []

        i = 0
        for line in config[key]["filters"]:
            filter_name = "Filter_%s" % key
            if len(config[key]["filters"]) > 1:
                filter_name += "_%d" % i
                i = i + 1

            tupleFilter = add_filter(filter_name, line)
            dictAlgs[key].append(tupleFilter)

        funTuple = Funtuple(
            name="Tuple_%s" % key,
            tuple_name=config[key]["tuple"],
            fields=config[key]["fields"],
            variables=config[key]["variables"],
            loki_preamble=config[key]["preamble"],
            inputs=inputs)

        dictAlgs[key].append(funTuple)

    return dictAlgs


def apply_algorithm(list_particles, algorithm, **kwargs):
    """
    Function that applies an algorithm, accepting a Particle::Range as input (e.g FilterDecays),
    to particles from Hlt2 or Spruce sample defined in the list of TES locations gived as argument.
    The extra arguments to algorithm can be passed via kwargs.

    Args:
        list_particles (list(str)): List of Transient event store (TES) location of Particles in Hlt2 or Spruce sample.
        algorithm (PyConf.Algorithms): Algorithm to be applied.
        kwargs : Extra arguments to the passed algorithm.
    Returns:
        Ouput TES location of the particles from the algorithm
    """
    dv_algorithm = make_dvalgorithm(algorithm)
    return dv_algorithm(ParticlesA=list_particles, **kwargs).Particles


def filter_on(location, decay_descriptor=None):
    """
    Function to get particles from Hlt2 or Spruce sample.
    A FilterDecays is applied before returning the requested particles if a decay descriptor
    is specified as extra argument.

    Args:
        location (str): Transient event store (TES) location of Particles in Hlt2 or Spruce sample
        decay_descriptor (str, optional): Decay descriptor that will be used to select decays in the event
    Returns:
        data: TES location of the particles that are loaded from the input samples
    """
    data = get_particles(location)
    if decay_descriptor:
        data = apply_algorithm([data], FilterDecays, Code=decay_descriptor)
    return data
