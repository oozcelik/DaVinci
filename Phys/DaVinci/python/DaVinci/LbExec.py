###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from contextlib import contextmanager
from typing import Optional
from GaudiConf.LbExec import Options as DefaultOptions, InputProcessTypes
from pydantic import root_validator
from PyConf.reading import (upfront_decoder, reconstruction, get_tes_root,
                            get_odin, get_hlt_reports, get_mc_track_info)
from PyConf.application import default_raw_event


class Options(DefaultOptions):
    """
  A class that holds the user-specified DaVinci options.

  This class inherits from the default `GaudiConf.LbExec.Options`.

  This class also configures several PyConf functions,
  see the list in the `apply_binds` method,
  where their keyword arguments are globally bound to the user-specified values.
  This way, users do not have to manually configure these functions themselves.

  The following user-required parameters need to be set in this class:
  - input_process (str): Input process type, see `GaudiConf.LbExec.options.InputProcessTypes` values.

  The optional parameters that need to be set are :
  - stream (str): Stream name. Default is "default"
        Note: for `input_process=Hlt2` the stream must be strictly empty. The default value is overwritten in this case.
  - lumi (bool): Flag to store luminosity information. Default is False.
  - evt_pre_filters (dict[str,str]): Event pre-filter code. Default is None.
  - annsvc_config (str): Path to the configuration file from sprucing or Hlt2. Default is None.
  - write_fsr (bool): Flag to write full stream record. Default is True.
  - merge_genfsr (bool): Flag to merge the full stream record. Default is False.
  - metainfo_additional_tags: (list): Additional central tags for `PyConf.filecontent_metadata.metainfo_repos`.
    Default is [].
  """
    input_process: InputProcessTypes
    stream: Optional[str] = 'default'
    lumi: bool = False
    evt_pre_filters: Optional[dict[str, str]] = None
    annsvc_config: Optional[str] = None
    write_fsr: bool = True
    merge_genfsr: bool = False
    metainfo_additional_tags: Optional[list] = []

    @root_validator(pre=False)
    def _stream_default(cls, values):
        """
      This is a validator that sets the default "stream" value based on "input_process"

      Args:
        values (dict): User-specified attributes of the Options object.

      Returns:
        dict: Modified attributes of the Options object.
      """
        input_process = values.get("input_process")
        if input_process == InputProcessTypes.Hlt2:
            values['stream'] = ''

        return values

    @contextmanager
    def apply_binds(self):
        """
        This function configures the following PyConf functions, where their keyword
        arguments are globally bound to the user-specified values:
        - default_raw_event
        - upfont_decoder
        - reconstruction
        - get_tes_root
        - get_odin
        - get_hlt_reports
        - get_mc_track_info
        This way users do not have to manually configure these functions themselves.
        """
        default_raw_event.global_bind(raw_event_format=self.input_raw_format)
        upfront_decoder.global_bind(
            input_process=self.input_process, stream=self.stream)
        reconstruction.global_bind(input_process=self.input_process)
        get_tes_root.global_bind(input_process=self.input_process)
        get_odin.global_bind(
            input_process=self.input_process, stream=self.stream)
        get_hlt_reports.global_bind(
            input_process=self.input_process, stream=self.stream)
        get_mc_track_info.global_bind(input_process=self.input_process)
        with super().apply_binds():
            yield
