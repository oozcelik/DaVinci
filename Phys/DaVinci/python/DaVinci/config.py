###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
DaVinci configured using PyConf components.
"""

import logging
from collections import namedtuple, OrderedDict
from Configurables import ApplicationMgr
from PyConf.application import configure, configure_input, configured_ann_svc
from PyConf.application import metainfo_repos
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import EventAccounting
from DaVinci.algorithms import (define_fsr_writer, apply_filters_and_unpacking)

log = logging.getLogger(__name__)


class DVNode(namedtuple('DVNode', ['node', 'extra_outputs'])):  # noqa
    """Immutable object fully qualifying a DaVinci node.
    Copied from the `HltLine` class without the prescaler argument.

    Attributes:
        node (CompositeNode): the control flow node of the line

    """
    __slots__ = ()  # do not add __dict__ (preserve immutability)

    def __new__(cls, name, algs, extra_outputs=None):
        """Initialize a DaVinci node from name and a set of algorithms.

        Creates a control flow `CompositeNode` with the given `algs`
        combined with `LAZY_AND` logic.

        Args:
            name (str): name of the line
            algs: iterable of algorithms
            extra_outputs (iterable of 2-tuple): List of (name, DataHandle) pairs.
        """
        node = CompositeNode(
            name,
            tuple(algs),
            combine_logic=NodeLogic.LAZY_AND,
            force_order=True)
        if extra_outputs is None:
            extra_outputs = []
        return super(DVNode, cls).__new__(cls, node, frozenset(extra_outputs))

    @property
    def name(self):
        """DVNode (CompositeNode instance) name."""
        return self.node.name


def davinci_control_flow(options, user_analysis_nodes=[], fsr_nodes=[]):
    """
    DaVinci control flow is split in a few sections as described in DaVinci/issue#2 (then simplified)

    .. code-block:: text

       DaVinci (LAZY_AND)
       ├── LuminosityNode (NONLAZY_OR)
       │   └── EventAccounting/EventAccount
       └── UserAnalysisNode (NONLAZY_OR)
           ├── DVUser1Node (LAZY_AND)
           │   ├── PVFilter
           │   ├── ParticlesFilter
           │   ├── Candidate1Combiner
           │   ├── AlgorithmsForTuple1
           │   ├── Tuple1
           └── DVUser2Node (LAZY_AND)
               ├── PVFilter
               ├── ParticlesFilter
               ├── Candidate2Combiner
               ├── AlgorithmsForTuple2
               └── Tuple2

    The main parts are

        * LuminosityNode
        * UserAnalysisNode

    who are in AND among each other and can accomodate nodes (defined with the class DVNode)
    in OR among themselves.

    To prepare the control flow there are a few options:
    
        1. take a dictionary as input where all the nodes are listed in their categories
        2. take a various lists as input each related to a specific category of nodes
    
    This function is then used to fill the control flow
    """
    options.finalize()

    dv_top_children = []

    ordered_nodes = OrderedDict()
    ordered_nodes['FileSummaryRecords'] = fsr_nodes
    ordered_nodes['UserAnalysis'] = user_analysis_nodes

    for k, v in ordered_nodes.items():
        if len(v):
            cnode = CompositeNode(
                k,
                combine_logic=NodeLogic.NONLAZY_OR,
                children=[dv_node.node for dv_node in v],
                force_order=False)
            dv_top_children += [cnode]

    return CompositeNode(
        'DaVinci',
        combine_logic=NodeLogic.LAZY_AND,
        children=dv_top_children,
        force_order=True)


def prepare_davinci_nodes(user_algs):
    """
    This helper function takes as input a dictionary of user algorithms in the form

    .. code-block:: python
    
        {
        'DVNode1' : [<list of algs>],
        'DVNode2' : [<list of algs>],
        }
    
    and creates the node to send to `davinci_control_flow`.
    """
    dv_nodes = []
    for k, algs in user_algs.items():
        if not type(algs) == list:
            raise TypeError("Dict values should all be lists of algorithms!")
        dv_nodes += [DVNode(k, algs)]
    return dv_nodes


def add_davinci_configurables(options, user_algorithms, public_tools):
    """
    Run the job adding the specific Davinci configurables to the standard PyConf ones.

    Algorithms developed by users are also included.

    Args:
        options (DaVinci.Options): lbexec provided options object

    Returns:
        ComponentConfig instance, a dict of configured Gaudi and DaVinci Configurable instances and user algorithms.
    """
    if not public_tools:
        public_tools = []

    config = configure_input(options)

    if options.input_manifest_file:
        if options.metainfo_additional_tags:
            metainfo_repos.global_bind(
                extra_central_tags=options.metainfo_additional_tags)

        ApplicationMgr().ExtSvc += [
            configured_ann_svc(json_file=options.input_manifest_file)
        ]

    elif options.annsvc_config:  # this should be renamed to input_manifest_file
        # distinguish between configuring the ANNSvc for decoding, and getting
        # the manifest to configure the unpacking (the latter has _NOTHING_ to
        # do with the ANNSvc)
        # Here we only do the decoding part. The unpacking manifest part we
        # do in apply_filters_and_unpacking...
        config.add(configured_ann_svc(json_file=options.annsvc_config))

    dvMainFlow = apply_filters_and_unpacking(options, user_algorithms)

    fsrAlgs = {}
    if options.write_fsr:
        if options.simulation:
            fsrAlgs.update({"GenFSR": define_fsr_writer(options)})

        if options.lumi:
            fsrAlgs.update({"Lumi": [EventAccounting(name='EventAccount')]})
            # this should be modified to reflect LumiAlgsConf (configured separately?)

    dvMainNode = davinci_control_flow(options,
                                      prepare_davinci_nodes(dvMainFlow),
                                      prepare_davinci_nodes(fsrAlgs))

    config.update(configure(options, dvMainNode, public_tools=public_tools))
    return config


def make_config(options, user_algorithms, *, public_tools=None):
    if isinstance(user_algorithms, list):
        user_algorithms = {"default": user_algorithms}

    return add_davinci_configurables(options, user_algorithms, public_tools)
