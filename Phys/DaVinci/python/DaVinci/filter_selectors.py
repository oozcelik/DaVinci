###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definitions of:

- Default cuts a la runs 1&2 common particles.
"""
from GaudiKernel.SystemOfUnits import MeV
import Functors as F
from Hlt2Conf.algorithms_thor import require_all

#################################
# Default particle cuts
#################################


def default_particle_cuts(pvs):
    """
    Return a string with the default particle standard loose cuts.
    """
    return require_all(F.PT > 250 * MeV, F.MINIPCHI2(pvs) > 4)
